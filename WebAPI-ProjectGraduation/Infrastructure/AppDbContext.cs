﻿using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using System.Threading.Channels;

namespace Infrastructure
{
    public class AppDbContext : DbContext
    {

        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options)
        {

        }
        public DbSet<User> Users { get; set; }
        public DbSet<Book> Books { get; set; }
        public DbSet<FavoriteBook> FavoriteBooks { get; set; }
        public DbSet<Review> Reviews { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<OrderDetail> OrderDetail { get; set; }
        public DbSet<CartItem> CartItem { get; set; }
        public DbSet<OTP> OTP { get; set; }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>()
                        .HasMany(o => o.Orders)
                        .WithOne(u => u.User)
                        .HasForeignKey(u => u.UserId);

            modelBuilder.Entity<User>()
                        .HasMany(user => user.FavoriteBooks)
                        .WithOne(favoriteBook => favoriteBook.User)
                        .HasForeignKey(favoriteBook => favoriteBook.UserId);

            modelBuilder.Entity<User>()
                        .HasMany(user => user.CartItems)
                        .WithOne(cart => cart.User)
                        .HasForeignKey(cart => cart.UserId);

            modelBuilder.Entity<Book>()
                        .HasMany(book => book.Reviews)
                        .WithOne(review => review.Book)
                        .HasForeignKey(review => review.BookId);

            modelBuilder.Entity<OrderDetail>()
                        .HasOne(od => od.Order)
                        .WithMany(order => order.OrderDetails)
                        .HasForeignKey(od => od.OrderId);

            modelBuilder.Entity<OrderDetail>()
                        .HasOne(od => od.Book)
                        .WithMany(book => book.OrderDetails)
                        .HasForeignKey(od => od.BookId);

            modelBuilder.Entity<FavoriteBook>().HasKey(sc => new { sc.UserId, sc.BookId });

            modelBuilder.Entity<OrderDetail>().HasKey(sc => new { sc.OrderId, sc.BookId });
        }
    }
}
