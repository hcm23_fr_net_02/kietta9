﻿using Application.ViewModels;
using AutoMapper;

namespace Infrastructure.Mappers
{
    public class MapperConfigurationsProfile : Profile
    {
        public MapperConfigurationsProfile() 
        {
            CreateMap<Book, BookViewModel>().ReverseMap();        
            CreateMap<Book, BookDTO>().ReverseMap();
            CreateMap<CartItem, CartItemAddDTO>().ReverseMap();
        }
    }
}
