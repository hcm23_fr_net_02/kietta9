﻿using Application.Interfaces;
using Application.IRepositories;

namespace Infrastructure.Repositories
{
    public class FavoriteBookRepository : GenericRepository<FavoriteBook>, IFavoriteBookRepository
    {
        private readonly AppDbContext _context;

        public FavoriteBookRepository(AppDbContext context, ICurrentTime timeService) : base(context, timeService)
        {
            _context = context;
        }
    }
}
