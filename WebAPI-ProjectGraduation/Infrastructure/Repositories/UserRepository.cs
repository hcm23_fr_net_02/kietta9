﻿using Application.Interfaces;
using Application.IRepositories;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure.Repositories
{
    public class UserRepository : GenericRepository<User>, IUserRepository
    {
        private readonly AppDbContext _context;
        public UserRepository(AppDbContext context, ICurrentTime timeService) : base(context, timeService)
        {
            _context = context;
        }

        public User GetUserByEmail(string email)
        {
            var user = _context.Users.FirstOrDefault(user => user.Email == email);
            if (user == null)
            {
                throw new Exception("Email incorrect!");
            }
            return user;
        }

        public async Task<User> GetUserByIdAsync(int id) =>  _context.Users.FirstOrDefault(user => user.Id == id);

        public async Task<User> GetUserByUsernameAndPasswordHash(string username, string password)
        {
            var user = await _context.Users.FirstOrDefaultAsync(record => record.Username == username
                                                                       && record.PasswordHash == password);
            if (user == null)
            {
                throw new Exception("Username or password incorrect!");
            }

            return user;
        }

        public Task<bool> IsExisted(string username) => _context.Users.AnyAsync(record => record.Username == username);

        public Task<bool> IsExistedEmail(string email) => _context.Users.AnyAsync(record => record.Email == email);
    }
}
