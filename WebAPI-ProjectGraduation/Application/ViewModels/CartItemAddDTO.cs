﻿namespace Application.ViewModels
{
    public class CartItemAddDTO
    {
        public int UserId { get; set; }
        public int BookId { get; set; }
        public int Quantity { get; set; }
    }
}
