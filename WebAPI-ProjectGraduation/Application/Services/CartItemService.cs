﻿using Application.Interfaces;
using Application.ViewModels;
using AutoMapper;

namespace Application.Services
{
    public class CartItemService : ICartItemService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public CartItemService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task AddBookToCart(CartItemAddDTO cartItemAddDTO)
        {
            var book = await _unitOfWork.BookRepository.GetBookByIdAsync(cartItemAddDTO.BookId);
            if (book is null)
            {
                throw new Exception($"Book with Id : {cartItemAddDTO.BookId} not found");
            }

            var user = await _unitOfWork.UserRepository.GetUserByIdAsync(cartItemAddDTO.UserId);
            if (user is null)
            {
                throw new Exception($"User with Id : {cartItemAddDTO.UserId} not found");
            }

            if (book.StockQuantity < cartItemAddDTO.Quantity || book.StockQuantity == 0)
            {
                throw new Exception($"Not enough Stock Quantity of Book: {book.Id}");
            }

            book.StockQuantity -= cartItemAddDTO.Quantity;

            ////var cartItem = new CartItem
            ////{
            ////    UserId = cartItemAddDTO.UserId,
            ////    BookId = cartItemAddDTO.BookId,
            ////    Quantity = cartItemAddDTO.Quantity
            ////};

            var cartItem = _mapper.Map<CartItem>(cartItemAddDTO);

            await _unitOfWork.CartItemRepository.AddAsync(cartItem);
            _unitOfWork.BookRepository.Update(book);

            await _unitOfWork.SaveChangesAsync();
        }


        public Task RemoveBookToCart(int BookId)
        {
            throw new NotImplementedException();
        }

        public Task ViewCartItemDetail()
        {
            throw new NotImplementedException();
        }
    }
}
