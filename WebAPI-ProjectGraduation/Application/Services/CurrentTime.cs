﻿using Application.Interfaces;
using System.Security.Cryptography.X509Certificates;

namespace Application.Services
{
    public class CurrentTime : ICurrentTime
    {
        public DateTime GetCurrentTime() => DateTime.UtcNow;
    }
}
