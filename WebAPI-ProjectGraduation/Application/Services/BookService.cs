﻿using Application.Interfaces;
using Application.ViewModels;
using AutoMapper;
using Domain.Enums;
using System.Net;

namespace Application.Services
{
    public class BookService : IBookService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly ICurrentTime _currentTime;

        public BookService(IUnitOfWork unitOfWork, IMapper mapper, ICurrentTime currentTime)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _currentTime = currentTime;
        }

        public async Task AddAsync(BookDTO bookAddDTO)
        {
            Book book = _mapper.Map<Book>(bookAddDTO);

            book.IsDeleted = false;
            
            await _unitOfWork.BookRepository.AddAsync(book);
            await _unitOfWork.SaveChangesAsync();
        }

        public async Task<List<Book>> GetBooksAsync()
        {
            var books = await _unitOfWork.BookRepository.GetAllAsync();
            return books;
        }

        public async Task<List<BookViewModel>> GetBooksByAuthorAsync(string author)
        {
            var books = await _unitOfWork.BookRepository.FindAsync(book => book.Author == author);
            if (books is null || !books.Any())
            {
                throw new Exception($"Books with Author: {author} can not be found!");
            }
            var results = _mapper.Map<List<BookViewModel>>(books);
            return results;
        }

        public async Task<List<BookViewModel>> GetBooksByGenreAsync(BookGenre genre)
        {
            var books = await _unitOfWork.BookRepository.FindAsync(book => book.Genre == genre);
            if (books is null || !books.Any())
            {
                throw new Exception($"Books with Genre: {genre} can not be found!");
            }
            var results = _mapper.Map<List<BookViewModel>>(books);
            return results;
        }

        public async Task<List<BookViewModel>> GetBooksByPublicationDateAsync(DateTime publicationDate)
        {
            var books = await _unitOfWork.BookRepository.FindAsync(book => book.PublicationDate == publicationDate);
            if (books is null || !books.Any())
            {
                throw new Exception($"Books with Publication Date: {publicationDate} can not be found!");
            }
            var results = _mapper.Map<List<BookViewModel>>(books);
            return results;
        }

        public async Task<List<BookViewModel>> GetBooksByTitleAsync(string title)
        {
            var books = await _unitOfWork.BookRepository.FindAsync(book => book.Title == title);
            if (books is null || !books.Any())
            {
                throw new Exception($"Books with Title: {title} can not be found!");
            }
            var results = _mapper.Map<List<BookViewModel>>(books);
            return results;
        }

        public async Task HardDelete(int bookId)
        {
            var book = await _unitOfWork.BookRepository.GetBookByIdAsync(bookId);
            if (book is null)
            {
                throw new Exception($"Book with Id {bookId} cannot be found!");
            }
            _unitOfWork.BookRepository.HardDelete(book);
            await _unitOfWork.SaveChangesAsync();
        }

        public async Task SoftDelete(int bookId)
        {
            var bookToDelete = await _unitOfWork.BookRepository.GetBookByIdAsync(bookId);
            if (bookToDelete is null)
            {
                throw new Exception($"Book with Id {bookId} cannot be found!");
            }
            _unitOfWork.BookRepository.SoftDelete(bookToDelete);
            await _unitOfWork.SaveChangesAsync();
        }

        public async Task UpdateBookAsync(int id, BookDTO book)
        {
            var updateBook = await _unitOfWork.BookRepository.GetBookByIdAsync(id);
            if (updateBook is null)
            {
                throw new Exception($"Book with Id {id} cannot be found!");
            }
            _mapper.Map<BookDTO>(updateBook);

            updateBook.Title = string.IsNullOrEmpty(book.Title) ? updateBook.Title : book.Title;
            updateBook.Author = string.IsNullOrEmpty(book.Author) ? updateBook.Author : book.Author;
            updateBook.PublicationDate = book.PublicationDate == default ? updateBook.PublicationDate : book.PublicationDate;
            updateBook.StockQuantity = book.StockQuantity == default ? updateBook.StockQuantity : book.StockQuantity;
            updateBook.PricePerUnit = book.PricePerUnit == default ? updateBook.PricePerUnit : book.PricePerUnit;
            updateBook.ModifiedOn = _currentTime.GetCurrentTime();

            await _unitOfWork.SaveChangesAsync();   
        }
    }
}
