﻿using Application.Commons;
using Application.Interfaces;
using Application.Utils;
using Application.ViewModels;
using Domain.Entities;
using Domain.Enums;
using System.Net;
using System.Net.Mail;

namespace Application.Services
{
    public class UserService : IUserService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly AppConfiguration _appConfiguration;
        private readonly ICurrentTime _currentTime;

        public UserService(IUnitOfWork unitOfWork, AppConfiguration appConfiguration, ICurrentTime currentTime)
        {
            _unitOfWork = unitOfWork;
            _appConfiguration = appConfiguration;
            _currentTime = currentTime;
        }

        public async Task<string> LoginAsync(UserLoginDTO user)
        {
            var userLogin = await _unitOfWork.UserRepository.GetUserByUsernameAndPasswordHash(user.Username, user.Password.Hash());
            return userLogin.GenerateJsonWebToken(_appConfiguration.JWTSection.SecretKey, _currentTime.GetCurrentTime());
        }

        public async Task RegisterAsync(UserRegisterDTO user)
        {
            var isExisted = await _unitOfWork.UserRepository.IsExisted(user.Username);

            if (isExisted)
            {
                throw new Exception("Username is already existed!");
            }

            var newUser = new User
            {
                Username = user.Username,
                PasswordHash = user.Password.Hash(),
                Name = user.Name,
                Address = user.Address,
                Email = user.Email,
                PhoneNumber = user.PhoneNumber,
                Role = Role.Customer
            };

            await _unitOfWork.UserRepository.AddAsync(newUser);
            await _unitOfWork.SaveChangesAsync();
        }


        public async Task ForgotPassword(ForgotPasswordDTO user)
        {
            var isExisted = await _unitOfWork.UserRepository.IsExistedEmail(user.Email);

            if (isExisted)
            {
                var otpCode = OTPUtils.GenerateOTP();
                var otp = new OTP
                {
                    Code = otpCode,
                    Email = user.Email,
                    CreatedAt = _currentTime.GetCurrentTime(),
                    ExpirationTime = _currentTime.GetCurrentTime().AddMinutes(5) //Một mã OTP tồn tại trong 5p 
                };
                await _unitOfWork.OTPRepository.AddAsync(otp);
                await _unitOfWork.SaveChangesAsync();

                OTPUtils.SendEmailAsync(user, otpCode);
            }
            else
            {
                throw new Exception("Email not found!");
            }
        }

        public async Task ResetPassword(string otp, string email, string newPassword)
        {
            // Kiểm tra xem OTP có hợp lệ không
            var validOTP = OTPUtils.GetValidOTP(email, otp, _unitOfWork, _currentTime);

            if (validOTP != null)
            {
                // Lấy người dùng dựa trên địa chỉ email
                var user = _unitOfWork.UserRepository.GetUserByEmail(email);
                if (user != null)
                {
                    // Đặt lại mật khẩu của người dùng
                    user.PasswordHash = newPassword.Hash();

                    // Xóa mã OTP đã sử dụng
                    _unitOfWork.OTPRepository.SoftDelete(validOTP);

                    //Lưu vào database
                    await _unitOfWork.SaveChangesAsync();
                }
                else
                {
                    throw new Exception("User not found!");
                }
            }
            else
            {
                throw new Exception("Invalid OTP");
            }
        }

        public async Task WalletRecharge(int money, int userId)
        {
            var user = await _unitOfWork.UserRepository.GetUserByIdAsync(userId);
            if (user is not null && money > 0)
            {
                user.WalletAmount += money;
                _unitOfWork.UserRepository.Update(user);
                await _unitOfWork.SaveChangesAsync();
            }
            else if (user is null)
            {
                throw new Exception($"User with Id {userId} not found!");
            }
            else if (money <= 0)
            {
                throw new Exception("Money add to wallet must be greater than 0$");
            }
        }
    }
}
