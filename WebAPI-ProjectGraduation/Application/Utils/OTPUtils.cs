﻿using System.Net.Mail;
using System.Net;
using System.Text;
using Application.ViewModels;
using Domain.Entities;
using Application.Interfaces;

namespace Application.Utils
{
    public static class OTPUtils
    {
        public static void SendEmailAsync(ForgotPasswordDTO forgotPassword, string otpCode)
        {
            string emailForSend = "truonganhkietdev@gmail.com";
            string appPasswordConfiguration = "pcpxpggokmexvyal";

            var smtpClient = new SmtpClient
            {
                Port = 587,
                EnableSsl = true,
                Host = "smtp.gmail.com",
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential(emailForSend, appPasswordConfiguration),
            };

            var message = new MailMessage()
            {
                Subject = "Reset password with OTP",
                Body = "Your OTP to Reset Password: " + otpCode,
                From = new MailAddress(emailForSend),
            };

            message.To.Add(new MailAddress(forgotPassword.Email));
            smtpClient.Send(message);

            forgotPassword.OTP = otpCode; // Lưu mã OTP vào ForgotPasswordDTO
        }

        public static OTP GetValidOTP(string email, string otp, IUnitOfWork _unitOfWork, ICurrentTime _currentTime)
        {
            // Kiểm tra xem mã OTP có hợp lệ không
            var validOTP = _unitOfWork.OTPRepository.GetOTPByEmailAndCode(email, otp);

            if (validOTP != null)
            {
                // Kiểm tra xem mã OTP đã hết hạn chưa
                if (validOTP.ExpirationTime >= _currentTime.GetCurrentTime())
                {
                    return validOTP; // Mã OTP hợp lệ
                }
            }

            return null; // Mã OTP không hợp lệ hoặc đã hết hạn
        }

        private static Random random = new Random();

        public static string GenerateOTP()
        {
            const string chars = "0123456789";

            StringBuilder otp = new StringBuilder(6);
            for (int i = 0; i < 6; i++)
            {
                otp.Append(chars[random.Next(chars.Length)]);
            }

            return otp.ToString();
        }
    }
}
