﻿namespace Application.IRepositories
{
    public interface IReviewRepository : IGenericRepository<Review>
    {
    }
}
