﻿namespace Application.IRepositories
{
    public interface IFavoriteBookRepository : IGenericRepository<FavoriteBook>
    {
    }
}
