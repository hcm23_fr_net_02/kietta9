﻿namespace Application.IRepositories
{
    public interface IUserRepository : IGenericRepository<User>
    {
        Task<User> GetUserByUsernameAndPasswordHash(string username, string password);
        User GetUserByEmail(string email);
        Task<User> GetUserByIdAsync(int id);
        Task<bool> IsExisted (string username);
        Task<bool> IsExistedEmail (string email);   
    }
}
