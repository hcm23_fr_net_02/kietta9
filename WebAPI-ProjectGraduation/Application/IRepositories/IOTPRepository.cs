﻿using Domain.Entities;

namespace Application.IRepositories
{
    public interface IOTPRepository : IGenericRepository<OTP>
    {
        public OTP GetOTPByEmailAndCode(string email, string code);
    }
}
