﻿namespace Application.IRepositories
{
    public interface IOrderRepository : IGenericRepository<Order>
    {
    }
}
