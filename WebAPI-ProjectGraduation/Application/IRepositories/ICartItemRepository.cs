﻿namespace Application.IRepositories
{
    public interface ICartItemRepository : IGenericRepository<CartItem>
    {
    }
}
