﻿namespace Application.IRepositories
{
    public interface IOrderDetailRepository : IGenericRepository<OrderDetail>   
    {
    }
}
