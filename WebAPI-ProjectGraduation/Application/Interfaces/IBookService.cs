﻿using Application.Commons;
using Application.ViewModels;
using Domain.Enums;

namespace Application.Interfaces
{
    public interface IBookService
    {
        Task AddAsync (BookDTO book);
        Task UpdateBookAsync(int id, BookDTO book);
        Task SoftDelete(int bookId);
        Task HardDelete(int bookId);
        Task<List<Book>> GetBooksAsync();
        Task<List<BookViewModel>> GetBooksByAuthorAsync(string author);
        Task<List<BookViewModel>> GetBooksByTitleAsync(string title);
        Task<List<BookViewModel>> GetBooksByGenreAsync(BookGenre genre);
        Task<List<BookViewModel>> GetBooksByPublicationDateAsync(DateTime publicationDate);
    }
}
