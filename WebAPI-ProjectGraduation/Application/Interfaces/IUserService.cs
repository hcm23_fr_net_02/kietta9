﻿using Application.ViewModels;

namespace Application.Interfaces
{
    public interface IUserService
    {
        public Task RegisterAsync(UserRegisterDTO user);
        public Task<string> LoginAsync(UserLoginDTO user);
        public Task ForgotPassword(ForgotPasswordDTO user);
        public Task ResetPassword(string OTP, string email, string newPassword);

        public Task WalletRecharge(int money, int userId);
    }
}
