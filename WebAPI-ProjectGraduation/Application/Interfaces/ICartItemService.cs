﻿using Application.ViewModels;

namespace Application.Interfaces
{
    public interface ICartItemService
    {
        Task AddBookToCart(CartItemAddDTO cartItemAddDTO);
        Task RemoveBookToCart(int bookId);
        Task ViewCartItemDetail();

    }
}
