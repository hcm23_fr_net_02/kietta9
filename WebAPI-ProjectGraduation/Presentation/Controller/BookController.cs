﻿using Application.Interfaces;
using Application.ViewModels;
using Domain.Enums;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Presentation.Controller
{
    public class BookController : BaseController
    {
        private readonly IBookService _bookService;

        public BookController(IBookService bookService)
        {
            _bookService = bookService;
        }

        [HttpPost]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> AddBookAsync([FromBody] BookDTO bookAddDTO)
        {
            try
            {
                await _bookService.AddAsync(bookAddDTO);
                return Ok("Added Book Successfully!");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPut("{id}")]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> UpdateBookAsync(int id, [FromBody] BookDTO bookAddDTO)
        {
            try
            {
                await _bookService.UpdateBookAsync(id, bookAddDTO);
                return Ok("Updated Book Successfully!");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }     
        }

        [HttpDelete("{id}")]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> DeleteBookAsync(int id)
        {
            try
            {
                await _bookService.SoftDelete(id);
                return Ok("Delete Book Successfully!");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            } 
        }

        [HttpDelete("{id}")]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> PermanentDeleteAsync(int id)
        {
            try
            {
                await _bookService.HardDelete(id);
                return Ok($"Permanent Delete Book with ID : {id}");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet]
        public async Task<IActionResult> GetAllBooksAsync()
        {
            try
            {
                var books = await _bookService.GetBooksAsync();
                return Ok(books.ToList());
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet]
        public async Task<IActionResult> GetBooksByTitleAsync([FromForm] string title)
        {
            try
            {
                var books = await _bookService.GetBooksByTitleAsync(title);
                return Ok(books);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet]
        public async Task<IActionResult> GetBooksByAuthorAsync([FromForm] string author)
        {
            try
            {
                var books = await _bookService.GetBooksByAuthorAsync(author);
                return Ok(books);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet]
        public async Task<IActionResult> GetBooksByGenreAsync([FromForm] BookGenre genre)
        {
            try
            {
                var books = await _bookService.GetBooksByGenreAsync(genre);
                return Ok(books);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet]
        public async Task<IActionResult> GetBooksByPublicationYear([FromForm] DateTime publicationYear)
        {
            try
            {
                var books = await _bookService.GetBooksByPublicationDateAsync(publicationYear);
                return Ok(books);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
