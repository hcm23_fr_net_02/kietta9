﻿using Application.Interfaces;
using Application.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Presentation.Controller
{
    public class UserController : BaseController
    {
        private readonly IUserService _userService;

        public UserController(IUserService userService)
        {
            _userService = userService;
        }

        [HttpPost]
        public async Task<IActionResult> RegisterAsync([FromBody] UserRegisterDTO userRegister)
        {
            try
            {
                await _userService.RegisterAsync(userRegister);
                return Ok("Register successfully!");
            }
            catch (Exception ex)
            {
                return BadRequest($"{ex.Message}");  
            }
        }

        [HttpPost]
        public async Task<IActionResult> LoginAsync([FromBody] UserLoginDTO userLogin)
        {
            try
            {
                var jwt = await _userService.LoginAsync(userLogin);
                return Ok(jwt);
            }
            catch (Exception ex)
            {
                return BadRequest($"{ex.Message}");
            }

        }

        [HttpPost]
        public async Task<IActionResult> ForgotPassword([FromBody] ForgotPasswordDTO user)
        {
            try
            { 
                await _userService.ForgotPassword(user);
                return Ok("An OTP email has been sent successfully!");
            }
            catch (Exception ex)
            {
                return BadRequest($"Failed to send OTP email: {ex.Message}");
            }
        }

        [HttpPost]
        public async Task<IActionResult> ResetPassword([FromBody] ForgotPasswordDTO resetPasswordDTO)
        {
            try
            {
                await _userService.ResetPassword(resetPasswordDTO.OTP, resetPasswordDTO.Email, resetPasswordDTO.NewPassword);
                return Ok("Reset Password successfully!");
            }
            catch (Exception ex)
            {
                return BadRequest($"{ex.Message}");
            }
        }

        [HttpPost("{userId}")]
        [Authorize(Roles = "Customer")]
        public async Task<IActionResult> WalletRecharge([FromForm] int money, int userId)
        {
            try
            {
                await _userService.WalletRecharge(money, userId);
                return Ok($"Add {money}$ for User {userId} successful!");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

    }
}
