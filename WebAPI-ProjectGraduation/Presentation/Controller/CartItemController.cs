﻿using Application.Interfaces;
using Application.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Security.Claims;

namespace Presentation.Controller
{
    public class CartItemController : BaseController
    {
        private readonly ICartItemService _cartItemService;

        public CartItemController(ICartItemService cartItemService)
        {
            _cartItemService = cartItemService;
        }

        [HttpPost]
        [Authorize(Roles = "Customer")]
        public async Task<IActionResult> AddBookToCartAsync([FromForm] CartItemAddDTO cartItemAddDTO)
        {
            try
            {
                var userId = HttpContext.User.FindFirst(ClaimTypes.NameIdentifier).Value;
                cartItemAddDTO.UserId = int.Parse(userId);
                await _cartItemService.AddBookToCart(cartItemAddDTO);
                return Ok($"Added {cartItemAddDTO.Quantity} of Book(ID: {cartItemAddDTO.BookId}) for User(ID: {cartItemAddDTO.UserId})");
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
