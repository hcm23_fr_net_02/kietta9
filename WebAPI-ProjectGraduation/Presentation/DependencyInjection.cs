﻿using Presentation.Middlewares;

namespace Presentation
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddWebAPIService (this IServiceCollection services)
        {
            services.AddControllers();

            services.AddSingleton<GlobalExceptionMiddleware>();

            services.AddHealthChecks();

            return services;
        }
    }
}
