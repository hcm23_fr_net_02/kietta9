﻿namespace Domain.Enums
{
    public enum BookGenre
    {
        Fantasy,
        Adventure,
        Romance,
        Thriller,
        Mystery
    }
}
