﻿public class Order : BaseEntity
{
    public int Id { get; set; }
    public int UserId { get; set; }
    public User User { get; set; }
    public ICollection<OrderDetail> OrderDetails { get; set; }
    public DateTime PaidDate { get; set; }
    public int TotalPrice { get; set; }
}

