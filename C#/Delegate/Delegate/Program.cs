﻿
//int Double(int number) => number * 2;
//int Triple(int number) => number * 3;
//int Square(int number) => number * number;
//int Cube(int number) => number * number * number;

//Console.Write("Enter number to calculate: ");
//int inputNumber = int.Parse(Console.ReadLine());

//Custom doubleCal = new Custom(Double);
//Custom tripleCal = new Custom(Double);
//Custom squreCal = new Custom(Double);
//Custom cubeCal = new Custom(Double);


//void PrintResult(int number, Custom cus)
//{
//    int result = cus(number);
//    Console.WriteLine("Result: " + result);
//}

//PrintResult(inputNumber, doubleCal);



//delegate int Custom(int number);

MathOperation add = Sum;
MathOperation substract = Substract;
MathOperation multiple = Multiple;
MathOperation division = Division;

int result1 = PerformOperation(1, 2, add);
Console.WriteLine(result1);
int result2 = PerformOperation(1, 2, substract);
Console.WriteLine(result2);
int result3 = PerformOperation(1, 2, multiple);
Console.WriteLine(result3);
int result4 = PerformOperation(1, 2, division);
Console.WriteLine(result4);

 
int Sum(int a, int b) => a + b;
int Substract(int a, int b) => a - b;
int Multiple(int a, int b) => a * b;
int Division(int a, int b) => a/b;
int PerformOperation(int a, int b, MathOperation operation)
{
    return operation(a, b);
}
delegate int MathOperation(int a, int b);
