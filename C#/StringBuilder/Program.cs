﻿//StringBuilder()
//- Là một class thuộc về namespace System.Text;
//- StringBuilder thể hiện cho Mutable, nghĩa là cho phép thêm, chèn, xóa và chỉnh sửa nội dung
//của chuỗi mà không cần tạo ra nhiều phiên bản mới của chuỗi.
// Một số method nổi bật Append(), Replace(), ToString()
using System.Text;

var sb = new StringBuilder();
sb.Append("Fresher");
sb.Append(".NET");

var sb2 = sb;
bool checkRef = ReferenceEquals(sb2, sb);
Console.WriteLine(checkRef);
