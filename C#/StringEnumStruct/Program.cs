﻿//Enum là một kiểu dữ liệu đặc biệt được sử dụng để định nghĩa một tập hợp các
//giá trị hằng số có ý nghĩa liên quan. Mỗi giá trị trong enum được gán một
//số nguyên tương ứng

Months currentMonth = Months.October;

switch (currentMonth)
{
    case Months.January:
        Console.WriteLine("Happy New Year");
        break;
    case Months.February:
        Console.WriteLine("Happy Lunar New Year");
        break;
    case Months.March:
        Console.WriteLine("Nothing special");
        break;
    case Months.April:
        Console.WriteLine("Happy 30th4 1th5");
        break;
    case Months.May:
        Console.WriteLine("Continue to party");
        break;
    case Months.June:
        Console.WriteLine("Happy Children day");
        break;
    case Months.July:
        Console.WriteLine("Happy Months idk");
        break;
    case Months.August:
        Console.WriteLine("Happy prepare for 2/9");
        break;
    case Months.September:
        Console.WriteLine("Happy 2/9");
        break;
    case Months.October:
        Console.WriteLine("Happy Women day");
        break;
    case Months.November:
        Console.WriteLine("Happy Teacher day");
        break;
    case Months.December:
        Console.WriteLine("merry Christmas");
        break;
    default:
        Console.WriteLine("Invalid Month");
        break;
}
public enum Months
{
    January,    // 0
    February,   // 1 -- Lunar new year
    March,      // 2
    April,      // 3
    May,        // 4
    June,       // 5
    July,       // 6
    August,     // 7
    September,  // 8
    October,    // 9  -- Trung thu 
    November,   // 10
    December    // 11  -- Giang Sinh
}

