﻿var dataPairWithIntString = new DataPair<int, string>(21, "Kiet");
Console.WriteLine(dataPairWithIntString.Data1);
Console.WriteLine(dataPairWithIntString.Data2);
Console.WriteLine("\n");

var dataPairWithDoubleBool = new DataPair<double, bool>(18.5, true);
Console.WriteLine(dataPairWithDoubleBool.Data1);
Console.WriteLine(dataPairWithDoubleBool.Data2);


public class DataPair<T, U>
{
    public T Data1;
    public U Data2;
    public DataPair(T data1, U data2)
    {
        Data1 = data1;
        Data2 = data2;
    }
}





//var test = new A<string, int>();
//test.a = "Kiet";
//test.b = 1;

//public class A<T, K>
//{
//    public T a { get; set; }
//    public K b { get; set; }
//}




//var intPair = new Pair<int>(10, 20);

//var stringPair = new Pair<string>("Fresher", ".NET");

//var charPair = new Pair<char>('a', 'b');

//var doublePair = new Pair<double>(10, 20);

//public class Pair<T>
//{
//    public T First;
//    public T Second;
//    public Pair(T a, T b)
//    {
//        First = a;
//        Second = b;
//    }
//}