﻿public class Menu
{
    public void Display()
    {
        Console.WriteLine("------------------------");
        Console.WriteLine("1. Add item into Stack");
        Console.WriteLine("2. Delete current Top item");
        Console.WriteLine("3. Return Top item");
        Console.WriteLine("4. Count items in Stack");
        Console.WriteLine("5. Clear Stack");
        Console.WriteLine("6. Exit");
        Console.Write("Your choice: ");
    }
}
