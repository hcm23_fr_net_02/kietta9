﻿public class Stack<T>
{
    T[] stack;
    int top = -1;
    int maxStackSize;
    public Stack(int maxStackSize)
    {
        this.maxStackSize = maxStackSize;
        stack = new T[maxStackSize];
    }

    public bool IsEmpty() => top == -1;

    public bool IsFull() => top == maxStackSize - 1;

    public void Push(T item)
    {
        if (IsFull())
        {
            throw new Exception("Stack is full");
        }
        top++;
        stack[top] = item;
    }

    public T Pop()
    {
        if (IsEmpty())
        {
            throw new Exception("Stack is empty");
        }
        T topValue = stack[top];
        top--;
        return topValue;

    }

    public T Peek() //return top value but not remove
    {
        if (IsEmpty())
        {
            throw new Exception("Stack is empty");
        }
        return stack[top];
    }
    public void Clear()
    {
        top = -1;
    }
    public int Count() => top + 1;
}
