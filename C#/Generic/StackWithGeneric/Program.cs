﻿Menu menu = new Menu();
int choice;

var stack = new Stack<int>(5);

do
{
    menu.Display();
    choice = int.Parse(Console.ReadLine());
    switch (choice)
    {
        case 1:
            Console.Write("Add item: ");
            if (int.TryParse(Console.ReadLine(), out int item))
            {
                stack.Push(item);
            }
            else
            {
                Console.WriteLine("Invalid input. Please enter an integer.");
            }
            break;
        case 2:
            if (!stack.IsEmpty())
            {
                Console.WriteLine("Removed top item: " + stack.Pop());
            }
            else
            {
                Console.WriteLine("Stack is empty.");
            }
            break;
        case 3:
            if (!stack.IsEmpty())
            {
                Console.WriteLine("Top item is: " + stack.Peek());
            }
            else
            {
                Console.WriteLine("Stack is empty.");
            }
            break;
        case 4:
            Console.WriteLine("Count of items in the stack: " + stack.Count());
            break;
        case 5:
            Console.WriteLine("Clearing the stack.");
            stack.Clear();
            break;
        case 6:
            return;
        default:
            Console.WriteLine("Invalid choice");
            break;
    }
} while (choice != 6);
