﻿//Get last item in an array
var a = new A();

int[] numbs = { 1, 2, 3, 4, 5 };
int lastNumb = a.GetLastItem(numbs);
Console.WriteLine($"Last number: {lastNumb}");

string[] str = { "Fresher", ".net", "FPT" };
string lastString = a.GetLastItem(str);
Console.WriteLine($"Last number: {lastString}");

public class A
{
    public T GetLastItem<T>(T[] arr)
    {
        if (arr == null)
        {
            throw new Exception("Null array");
        }
        return arr[arr.Length - 1];
    }
}