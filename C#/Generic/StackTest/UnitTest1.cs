namespace StackTest
{
    public class UnitTest1
    {
        [Fact]
        public void Test1()
        {
            //Arrange
            var stack = new Stack<int>(5);
            var item = 1;
            stack.Push(item);
            var expected = 1;
            //Act
            var actual = stack.Count();
            //Assert
            Assert.Equal(expected, actual); 

        }
        [Fact]
        public void Test2()
        {
            //Arrange
            var stack = new Stack<int>(5);           
            var expected = true;
            //Act
            var actual = stack.IsEmpty();
            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Test3()
        {
            //Arrange
            var stack = new Stack<int>(5);
            stack.Push(5);
            stack.Push(10);
            stack.Push(15);
            stack.Pop();
            var expected = 2;
            //Act
            var actual = stack.Count();
            //Assert
            Assert.Equal(expected, actual);
        }
    }
}