﻿Menu menu = new Menu();
DateTimeExcersise excersise = new DateTimeExcersise();
int choice;
do
{
    menu.Display();
    choice = int.Parse(Console.ReadLine());
    switch (choice)
    {
        case 1:
            excersise.CalculateDaysBetweenDates();
            break;
        case 2:
            excersise.DetermineDayOfWeek();
            break;
        case 3:
            excersise.CalculateAgeAndZodiacSign();
            break;
        default:
            Console.WriteLine("Invalid choice");
            break;
    }

} while (choice > 4);
