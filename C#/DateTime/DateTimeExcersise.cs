﻿public class DateTimeExcersise
{
    public void CalculateDaysBetweenDates()
    {
        Console.WriteLine("Nhập ngày thứ nhất (dd/mm/yyy): ");
        DateTime date1 = DateTime.ParseExact(Console.ReadLine(), "dd/MM/yyyy", null);

        Console.Write("Nhập ngày thứ hai (dd/mm/yyyy): ");
        DateTime date2 = DateTime.ParseExact(Console.ReadLine(), "dd/MM/yyyy", null);

        TimeSpan span = date2.Subtract(date1);
        Console.WriteLine("Số ngày chênh lệch giữa hai ngày là: " + span.Days);
    }

    public void DetermineDayOfWeek()
    {
        Console.Write("Nhập ngày (dd/mm/yyyy): ");
        DateTime date = DateTime.ParseExact(Console.ReadLine(), "dd/MM/yyyy", null);

        Console.WriteLine("Ngày này là thứ: " + ((int)date.DayOfWeek + 1));
    }

    public void CalculateAgeAndZodiacSign()
    {
        Console.Write("Nhập ngày sinh (dd/mm/yyyy): ");
        DateTime birthDate = DateTime.ParseExact(Console.ReadLine(), "dd/MM/yyyy", null);

        int age = DateTime.Today.Year - birthDate.Year;
        if (birthDate > DateTime.Today.AddYears(-age)) age--;

        string zodiacSign = "";

        switch (birthDate.Month)
        {
            case 1:
                zodiacSign = (birthDate.Day <= 19) ? "Ma Kết" : "Bảo Bình";
                break;
            case 2:
                zodiacSign = (birthDate.Day <= 18) ? "Bảo Bình" : "Song Ngư";
                break;
            case 3:
                zodiacSign = (birthDate.Day <= 20) ? "Song Ngư" : "Bạch Dương";
                break;
            case 4:
                zodiacSign = (birthDate.Day <= 20) ? "Bạch Dương" : "Kim Ngưu";
                break;
            case 5:
                zodiacSign = (birthDate.Day <= 20) ? "Kim Ngưu" : "Song Tử";
                break;
            case 6:
                zodiacSign = (birthDate.Day <= 21) ? "Song Tử" : "Cự Giải";
                break;
            case 7:
                zodiacSign = (birthDate.Day <= 22) ? "Cự Giải" : "Sư Tử";
                break;
            case 8:
                zodiacSign = (birthDate.Day <= 22) ? "Sư Tử" : "Xử Nữ";
                break;
            case 9:
                zodiacSign = (birthDate.Day <= 22) ? "Xử Nữ" : "Thiên Bình";
                break;
            case 10:
                zodiacSign = (birthDate.Day <= 23) ? "Thiên Bình" : "Bọ Cạp";
                break;
            case 11:
                zodiacSign = (birthDate.Day <= 21) ? "Bọ Cạp" : "Nhân Mã";
                break;
            case 12:
                zodiacSign = (birthDate.Day <= 21) ? "Nhân Mã" : "Ma Kết";
                break;
            default:
                break;
        }
        Console.WriteLine("Tuổi của bạn là: " + age);
        Console.WriteLine("Cung hoàng đạo của bạn là: " + zodiacSign);
    }
}
