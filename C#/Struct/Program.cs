﻿//Struct là một kiểu dữ liệu tương tự như class, bao gồm các fields và members được nhóm lại thành một đối tượng đơn giản
//một số đặc điểm của struct:
//1. Struct là Value type 
//2. Struct không hỗ trợ tính kế thừa 
//3. Không thể có constructor không tham số. Một đối tượng struct phải được khởi tạo trước khi sử dụng


// Khởi tạo một biến kiểu Point
Point point = new Point(10, 20);

// Truy cập các thành phần dữ liệu của struct
int x = point.X; // x = 10
int y = point.Y; // y = 20

// Biến point là một bản sao của struct Point, không phải là tham chiếu
Point anotherPoint = point;
anotherPoint.X = 30;

Console.WriteLine($"point.X = {point.X}, point.Y = {point.Y}");
Console.WriteLine($"anotherPoint.X = {anotherPoint.X}, anotherPoint.Y = {anotherPoint.Y}");

// Định nghĩa một struct tên là "Point"
public struct Point
{
    public int X;
    public int Y;
    public Point(int x, int y)
    {
        X = x;
        Y = y;
    }
}

       

