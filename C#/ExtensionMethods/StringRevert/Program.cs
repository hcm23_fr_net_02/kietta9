﻿//Hãy tạo một extension method cho lớp string có tên là DaoChuoi, để thực hiện đảo
//ngược chuỗi ký tự. Yêu cầu: Nhận vào một chuỗi ký tự và trả về chuỗi ký tự đã được đảo ngược.
//Ví dụ:
//string text = "Hello, world!";
//string reversedText = text.DaoChuoi(); --> Kết quả: "!dlrow ,olleH"
// Console.WriteLine(reversedText);

string text = "Hello, world!";
string reversedText = text.DaoChuoi();
Console.WriteLine(reversedText);
public static class MyExtension
{
    public static string DaoChuoi(this string inputString)
    {

        char[] chars = inputString.ToCharArray();   //Convert string input into an array of char
        Array.Reverse(chars);                       //Reverse array chars
        string reverseStr = new string(chars);      //Create a new string of chars
        return reverseStr;
    }
}