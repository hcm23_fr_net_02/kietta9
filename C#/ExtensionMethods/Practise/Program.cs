﻿//1.Tạo một lớp static có tên là MyExtensions.
//2. Trong lớp MyExtensions, viết một phương thức mở rộng cho kiểu int có
//tên là Square, cho phép tính bình phương của một số nguyên.
//3. Phương thức Square có tham số đầu vào là một số nguyên và trả về kết
//quả là bình phương của số đó.
//4. Sử dụng phương thức Square trong chương trình chính để tính bình
//phương của một số nguyên và hiển thị 

int numb = 5;
int result = numb.Square();
Console.WriteLine(result);

public static class MyExtensions
{
    public static int Square(this int numb)
    {
        return numb * numb;
    }
}