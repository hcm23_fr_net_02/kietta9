﻿//Hãy tạo một extension method cho lớp String có tên là RemoveDuplicates, để loại bỏ các ký
//tự trùng lặp trong một chuỗi. Yêu cầu: Nhận vào một chuỗi và trả về một chuỗi mới đã loại bỏ các ký tự trùng lặp. Chuỗi
//kết quả không được sắp xếp lại, mà chỉ giữ lại các ký tự duy nhất theo thứ tự xuất hiện đầu
//tiên

using System.Text;


string input = "Hello World";
string result = input.RemoveDuplicates(); // Kết quả: "Helo Wrd"
Console.WriteLine(result);
public static class MyExtension
{
    public static string RemoveDuplicates(this string str)
    {
        if (string.IsNullOrEmpty(str))
        {
            return str; // Trả về chuỗi ban đầu nếu rỗng hoặc null
        }

        StringBuilder result = new StringBuilder();
        HashSet<char> seenCharacters = new HashSet<char>();

        foreach (char character in str)
        {
            if (!seenCharacters.Contains(character))
            {
                result.Append(character);
                seenCharacters.Add(character);
            }
        }

        return result.ToString();

    }
}