﻿int s1 = 5;
int sum = s1.Sum(10);
Console.WriteLine(sum);

int s2 = 100;
int substract = s2.Substract(99);
Console.WriteLine(substract);

string str = "Anh Kiet";
string print = str.PrintOut(); // this trong hàm PrintOut() chỉ định biến được mở rộng, PrintOut() không có tham số truyền vào
Console.WriteLine(print);

public static class OperationExtension
{
    public static int Sum(this int a, int b)
    {
        return a + b;
    }

    public static int Substract(this int a, int b)
    {
        return a - b;
    }

    public static string PrintOut(this string str)
    {
        return str;
    }
}