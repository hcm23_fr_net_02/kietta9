﻿//Hãy tạo một extension method cho class List<int> có tên là GetAverage, để tính giá trị trung
//bình của các phần tử trong danh sách. Yêu cầu: Nhận vào một danh sách số nguyên và trả về giá
//trị trung bình của các phần tử trong danh sách

List<int> numbers = new List<int> { 1, 2, 3, 4, 5 };
double average = numbers.GetAverage(); // Kết quả: 3.0
Console.WriteLine(average);
public static class ListExtension
{
    public static double GetAverage(this List<int> numbers)
    {
        if (numbers == null || numbers.Count == 0)
        {
            throw new Exception("List empty or null");
        }

        double average = numbers.Average();
        return average;
    }
}