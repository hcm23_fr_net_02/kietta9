﻿//String Concept
//- string là một reference type 
//- string mang tính chất Immutable, nghĩa là không thể thay đổi giá trị sau khi khởi tạo. 
//Bất kỳ thay đổi nào trên chuỗi này đều tạo ra một chuỗi mới
//- Sự không thay đổi này giúp bảo đảm tính nhất quán và an toàn trong việc làm việc với chuỗi,
//nhưng có thể gây ra hiệu suất kém khi bạn thực hiện nhiều thay đổi trên chuỗi lớn.

//Một số Properties nổi bật của string
//1.Length
Console.Write("Input random string: ");
string str = Console.ReadLine();
int length = str.Length;
Console.WriteLine($"Do dai cua chuoi {str} la {length} ky tu");
Console.ReadKey();

//2. IsNullOrEmpty
string str1 = "Kietta9";
string str2 = null;
bool isEmptyOrNull1 = string.IsNullOrEmpty(str1);
Console.WriteLine(isEmptyOrNull1);
bool isEmptyOrNull2 = string.IsNullOrEmpty(str2);
Console.WriteLine(isEmptyOrNull2);

//3. Substring
string str = "Fresher .NET";
string subStr = str.Substring(0, 7);
Console.WriteLine(subStr);

//4. Concat
string s1 = "Fresher";
string s2 = ".NET";
string s3 = string.Concat(s1, " ", s2);
Console.WriteLine(s3);

//Một số Method nổi bật của String
//1.Contains()-- > Kiểm tra một chuỗi bất kỳ có nằm trong chuỗi được chỉ định hay không
string sentence = "This is a sample sentence.";
bool containsWord = sentence.Contains("sampleaa"); // containsWord = true
Console.WriteLine(containsWord);

//2. Replace() --> Thay thế một đoạn chuỗi mới vào một đoạn chuỗi cũ
string sentence = "This is a sample sentence.";
string replacedSentence = sentence.Replace("sample", "new");
Console.WriteLine(replacedSentence);

//3. Split() --> Tách chuỗi thành một mảng các phần tử con được lấy từ chuỗi gốc 
string str = "Fresher,NET,FPT,Academy";
string[] strArr = str.Split(',');
foreach (string strArrItem in strArr)
{
    Console.WriteLine(strArrItem);
}

//4. Join() --> Kết hợp các phần tử từ một mảng thành một chuỗi hoàn chỉnh
string[] str = { "Fresher", ".NET", "FPT", "Academy" };
string strJoin = string.Join(" - ", str);
Console.WriteLine(strJoin);
Console.ReadKey();

//---> Ngoài ra còn có một số Method khác như ToUpper và ToLower, IndexOf và LastIndexOf, StartsWith và EndsWith


