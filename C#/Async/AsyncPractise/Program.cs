﻿//Bài tập: Mô phỏng đồng hồ đếm ngược
//1. Yêu cầu người dùng nhập thời gian đếm ngược mong muốn theo đơn vị giây.
//2. Bắt đầu đếm ngược từ thời gian đó về 0.
//3. Mỗi giây, chương trình sẽ hiển thị thời gian còn lại trên console.
//4. Tạo menu cho người dùng có thể chọn đếm ngược và kết thúc chương trình
//Ví dụ, nếu người dùng nhập 30, chương trình sẽ đếm ngược từ 30 về 0 và hiển
//thị thời gian còn lại trên console mỗi giây

Console.Write("Input second to countdown: ");
int second = int.Parse(Console.ReadLine());
var count1 = CountDown(second);
await count1;
async Task CountDown(int seconds)
{
    for (int i = seconds; i >= 0; i--)
    {
        Console.WriteLine($"Remaining time: {i} seconds");
        await Task.Delay(1000); // chờ bất đồng bộ
    }
    Console.WriteLine("Done!");
}
