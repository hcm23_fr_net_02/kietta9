﻿public class UrlManager
{
    private List<string> urls = new List<string>();
    private static readonly HttpClient client = new HttpClient();
    const string savePath = "img";

    public void Add(string url)
    {
        try
        {
            urls.Add(url);
        }
        catch (Exception)
        {
            Console.WriteLine("Added fail"); ;
        }
    }

    public List<string> GetUrls()
    {
        return urls;
    }

    public async Task DownloadImageAsync(string url)
    {
        try
        {
            HttpResponseMessage response = await client.GetAsync(url);
            if (response.IsSuccessStatusCode)
            {
                byte[] imageBytes = await response.Content.ReadAsByteArrayAsync();

                // Extract the file name from the URL and combine it with the save path
                string fileName = Path.GetFileName(new Uri(url).LocalPath);
                string filePath = Path.Combine(savePath, fileName);

                System.IO.File.WriteAllBytes(filePath, imageBytes);

                Console.WriteLine($"Downloaded successful and saved to: {filePath}");
            }
            else
            {
                Console.WriteLine($"Error: {response.StatusCode}");
            }
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Error: {ex.Message}");
        }
    }

    public async Task DownloadAllImagesAsync()
    {
        List<Task> downloadTasks = new List<Task>();

        foreach (string url in urls)
        {
            downloadTasks.Add(DownloadImageAsync(url));
        }
        await Task.WhenAll(downloadTasks);
    }
}
