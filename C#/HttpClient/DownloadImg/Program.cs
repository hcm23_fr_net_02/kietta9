﻿Menu menu = new Menu();
UrlManager urlManager = new UrlManager();
int choice;
do
{
    menu.Display();
    choice = int.Parse(Console.ReadLine());
    switch (choice)
    {
        case 1:
            Console.Write("Input URL: ");
            string url = Console.ReadLine();
            urlManager.Add(url);
            break;
        case 2:
            Console.WriteLine("Show all Urls: ");
            List<string> urls = urlManager.GetUrls();
            foreach (string u in urls)
            {
                Console.WriteLine(u);
                Console.WriteLine("\n");
            }
            break;
        case 3:
            Console.Write("Input Url and save imgage to folder:");
            string urlImg = Console.ReadLine();
            await urlManager.DownloadImageAsync(urlImg);
            break;
        case 4:
            Console.WriteLine("Downloading all images from URL List...");
            await urlManager.DownloadAllImagesAsync();
            Console.WriteLine("All images downloaded.");
            break;
        case 5:
            return;
        default:
            Console.WriteLine("Invalid choice");
            break;
    }

} while (choice != 5);
