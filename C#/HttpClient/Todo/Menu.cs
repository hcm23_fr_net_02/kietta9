﻿public class Menu
{
    public void Display()
    {
        Console.WriteLine("1. Show all Todo list");
        Console.WriteLine("2. Add new Todo");
        Console.WriteLine("3. Update Todo's status");
        Console.WriteLine("4. Change Todo's priority");
        Console.WriteLine("5. Exit");
        Console.Write("Your choice: ");
    }

    public void SubMenu()
    {
        Console.WriteLine("1. Order Todo by Priority");
        Console.WriteLine("2. Order Todo by Create date");
        Console.WriteLine("3. Return to Main menu");
        Console.Write("Your choice: ");
    }
}