﻿public class TodoManager
{
    List<Todo> list = new List<Todo>();

    public List<Todo> ViewTodosByPriority()
    {
        var orderedList = list.OrderBy(x => x.Priority).ToList();
        foreach (var item in orderedList)
        {
            Console.WriteLine(item);
        }
        return orderedList;
    }

    public List<Todo> ViewTodosByCreateAt()
    {
        var orderedList = list.OrderBy(x => x.CreatedAt).ToList();
        foreach (var item in orderedList)
        {
            Console.WriteLine(item);
        }
        return orderedList;
    }

    public void Add(Todo todo)
    {
        Console.Write("Id: ");
        todo.Id = int.Parse(Console.ReadLine());
        Console.Write("Title: ");
        todo.Title = Console.ReadLine();
        Console.Write("Description: ");
        todo.Description = Console.ReadLine();
        Console.Write("Create at: ");
        todo.CreatedAt = DateTime.Parse(Console.ReadLine());
        Console.Write("Priority: ");
        todo.Priority = int.Parse(Console.ReadLine());
        list.Add(todo);
        Console.WriteLine("Added!");
    }

    public void Delete(int id)
    {
        var deleteTodo = list.FirstOrDefault(x => x.Id == id);
        if (deleteTodo != null)
        {
            list.Remove(deleteTodo);
        }
        else
        {
            Console.WriteLine("Invalid ID");
        }
    }

    public void UpdateStatus(int id)
    {
        var changeStatusTodo = list.FirstOrDefault(x => x.Id == id);
        if (changeStatusTodo != null)
        {
            changeStatusTodo.IsCompleted = true;
        }
        else
        {
            Console.WriteLine("Invalid ID");
        }
    }

    public void ChangePriority(int id)
    {
        var changePriorityTodo = list.FirstOrDefault(x => x.Id == id);
        if (changePriorityTodo != null)
        {
            Console.Write("Update priority: ");
            changePriorityTodo.Priority = int.Parse(Console.ReadLine());
        }
        else
        {
            Console.WriteLine("Invalid ID");
        }
    }
}
