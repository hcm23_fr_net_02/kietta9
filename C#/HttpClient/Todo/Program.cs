﻿Menu menu = new Menu();
TodoManager todoManager = new TodoManager();
int choice;
do
{
    menu.Display();
    choice = int.Parse(Console.ReadLine());
    switch (choice)
    {
        case 1:
            menu.SubMenu();
            do
            {
                switch (choice)
                {
                    case 1:
                        todoManager.ViewTodosByPriority();
                        break;
                    case 2:
                        todoManager.ViewTodosByCreateAt();
                        break;
                    case 3:
                        Console.WriteLine("Return to Main Menu...");
                        return;
                    default:
                        Console.WriteLine("Invalid choice");
                        break;
                }
            } while (choice != 3);

            break;
        case 2:
            todoManager.Add(new Todo());
            break;
        case 3:
            Console.WriteLine("Enter Todo's Id to delete");
            int id = int.Parse(Console.ReadLine());
            todoManager.Delete(id);
            break;
        case 4:
            Console.WriteLine("Enter Todo's Id to update status");
            int status = int.Parse(Console.ReadLine());
            todoManager.UpdateStatus(status);
            break;
        case 5:
            Console.WriteLine("Enter Todo's Id to change priority");
            int priority = int.Parse(Console.ReadLine());
            todoManager.ChangePriority(priority);
            break;
        case 6:
            return;
        default:
            break;
    }
} while (choice != 6);
