﻿class Menu
{
    public void Display()
    {
        Console.WriteLine("------Menu-chính------");
        Console.WriteLine("1. Thêm sinh viên");
        Console.WriteLine("2. Xem danh sách sinh viên");
        Console.WriteLine("3. Tìm sinh viên theo ID");
        Console.WriteLine("4. Xóa sinh viên");
        Console.WriteLine("5. Thoát chương trình");
        Console.Write("Bạn chọn: ");
    }

    public void Paging()
    {
        Console.WriteLine("------Xem-danh-sách-sinh-viên------");
        Console.WriteLine("1. Trang trước");
        Console.WriteLine("2. Trang tiếp theo");
        Console.WriteLine("3. Quay lại menu chính");
        Console.Write("Bạn chọn: ");
    }
}
