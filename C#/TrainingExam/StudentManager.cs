﻿class StudentManager
{
    public const int PageSize = 5;
    public List<Student> Students = new List<Student>();

    public void Add(Student student)
    {
        try
        {
            Console.Write("Nhập Tên: ");
            student.Name = Console.ReadLine();

            int age;
            do
            {
                Console.Write("Nhập Tuổi (không âm): ");
            } while (!int.TryParse(Console.ReadLine(), out age) || age < 0);
            student.Age = age;

            double gpa;
            do
            {
                Console.Write("Nhập GPA(0 < GPA < 10): ");
            } while (!double.TryParse(Console.ReadLine(), out gpa) || gpa < 0 || gpa > 10);
            student.GPA = gpa;

            student.Id = Students.Count + 1;

            Students.Add(student);
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Lỗi: {ex.Message}");
        }
    }

    public void ViewStudents(int page)
    {
        int startIndex = (page - 1) * PageSize;
        int endIndex = Math.Min(startIndex + PageSize, Students.Count);

        if (Students.Count > 0)
        {
            Console.WriteLine($"------Danh sách sinh viên - Trang {page}------");
            for (int i = startIndex; i < endIndex; i++)
            {
                var student = Students[i];
                Console.WriteLine($"ID: {student.Id} | Tên: {student.Name} | Tuổi: {student.Age} | GPA: {student.GPA}");
            }
        }
        else
        {
            Console.WriteLine("Không có sinh viên nào để hiển thị");
        }
    }

    public Student FindStudentById(int id)
    {
        var targetFind = Students.FirstOrDefault(x => x.Id == id);
        if (targetFind != null)
        {
            Console.WriteLine("Thông tin sinh viên");
            Console.WriteLine($"ID: {targetFind.Id} | Tên: {targetFind.Name} | Tuổi: {targetFind.Age} | GPA: {targetFind.GPA}");
        }
        else
        {
            Console.WriteLine("Không tìm thấy sinh viên!");
        }
        return targetFind;
    }

    public void Delete(int id)
    {
        var targetDelete = Students.FirstOrDefault(x => x.Id == id);
        if (targetDelete != null)
        {
            Students.Remove(targetDelete);
            Console.WriteLine($"Xóa sinh viên ID {targetDelete.Id} thành công!");
        }
        else
        {
            Console.WriteLine("Không tìm thấy sinh viên!");
        }
    }
}
