﻿Console.OutputEncoding = System.Text.Encoding.UTF8;
StudentManager studentManager = new StudentManager();
Menu menu = new Menu();
int page = 1;
int choice;
try
{
    do
    {
        menu.Display();
        choice = int.Parse(Console.ReadLine());
        switch (choice)
        {
            case 1:
                Student student = new Student();
                studentManager.Add(student);
                Console.Clear();
                break;
            case 2:
                int pageChoice;
                do
                {
                    menu.Paging();
                    pageChoice = int.Parse(Console.ReadLine());
                    switch (pageChoice)
                    {
                        case 1:
                            if (page > 1)
                            {
                                page--;
                                studentManager.ViewStudents(page);
                            }
                            else
                            {
                                Console.WriteLine("Bạn đang ở trang đầu tiên");
                            }
                            break;
                        case 2:
                            if (page * StudentManager.PageSize < studentManager.Students.Count)
                            {
                                page++;
                                studentManager.ViewStudents(page);
                            }
                            else
                            {
                                Console.WriteLine("Bạn đang ở trang cuối cùng");
                            }
                            break;
                        case 3:
                            break;
                        default:
                            Console.WriteLine("Lựa chọn không phù hợp!");
                            break;
                    }
                } while (pageChoice != 3);
                Console.Clear();
                break;
            case 3:
                Console.Write("Nhập ID Sinh viên: ");
                int id = int.Parse(Console.ReadLine());
                studentManager.FindStudentById(id);
                break;
            case 4:
                Console.Write("Nhập ID Sinh viên: ");
                int idDel = int.Parse(Console.ReadLine());
                studentManager.Delete(idDel);
                break;
            case 5:
                return;
            default:
                Console.WriteLine("Lựa chọn không phù hợp!");
                break;
        }
    } while (choice != 5);
}
catch (Exception ex)
{
    Console.WriteLine($"Lỗi: {ex.Message}");
}
