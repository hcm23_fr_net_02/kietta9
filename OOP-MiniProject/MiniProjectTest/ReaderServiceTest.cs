﻿namespace MiniProjectTest
{
    public class ReaderServiceTest
    {
        #region Setup
        private readonly IReaderRepository _readerRepository;
        private readonly IReaderService _readerService;

        public ReaderServiceTest()
        {
            _readerRepository = new ReaderRepository("readers.json");
            _readerService = new ReaderService(_readerRepository);
            _readerRepository.LoadData();
        }
        #endregion

        #region Test GetAll
        [Fact]
        public void GetAllReaders_ReturnTrueNumberOfReader()
        {
            //Arrange
            var expected = 25;

            //Act
            var actual = _readerRepository.GetAll().Count();

            //Assert
            Assert.Equal(expected, actual);
        }
        #endregion

        #region Test ReaderById
        [Fact]
        public void GetReaderByID_ReturnIfIdExist()
        {
            //Arrange
            var reader = new Reader() { Id = 1, Name = "Kiet", Address = "abc", PhoneNumber = "123-456" };

            //Act
            var actual = _readerRepository.GetReader(1);

            //Assert
            Assert.Equal(reader.Id, actual.Id);
        }
        #endregion

        #region Test AddReader
        [Fact]
        public void AddReder_ReturnAddedSuccessfully()
        {
            //Arange
            var expected = 26; //Number of List<Reader> after added
            var reader = new Reader() { Id = 26, Name = "Kiet", Address = "abc", PhoneNumber = "123-456" };

            //Act
            _readerRepository.Add(reader);
            int actual = _readerRepository.GetAll().Count();

            //Assert
            Assert.Equal(expected, actual);
        }
        #endregion

        #region Test UpdateReader
        [Fact]
        public void UpdateReader_ReturnUpdatedSuccessfully()
        {
            var reader = new Reader() { Id = 1, Name = "Kiet", Address = "FPT", PhoneNumber = "123-456" };

            var reader2 = _readerRepository.GetReader(reader.Id);
            var name = reader2.Name;
            reader2.Name = reader.Name;
            _readerService.Update(1, reader2);

            Assert.NotEqual(name, reader2.Name);
        }
        #endregion

        #region Test DeleteReader
        [Fact]
        public void DeleteReader_ReturnDeletedSuccessfully()
        {
            var expected = 25;

            _readerService.Delete(1);
            var actual = _readerRepository.GetAll().Count();

            Assert.Equal(expected, actual);
        }
        #endregion
    }
}
