namespace MiniProjectTest
{
    public class BookServiceTest
    {
        #region Setup
        private readonly IBookRepository _repository;
        private readonly BookService _bookService;
        public BookServiceTest()
        {
            _repository = new BookRepository("books.json");
            _repository.LoadData();

        }
        #endregion

        #region Test Get All
        [Fact]
        public void GetAll_ReturnsCorrectNumberOfBook()
        {
            //Arrange
            var expected = 25;

            //Act
            IEnumerable<Book> list = _repository.GetAll();
            int actual = list.Count();

            //Assert
            Assert.Equal(expected, actual);
        }
        #endregion

        #region Test If BookID exist
        [Fact]
        public void GetBook_ReturnTrueIfIdExist()
        {
            // Arrange
            var expectedBook = new Book { Id = 1, Title = "ABC", Author = "ABC", PublicationYear = 2000, Quantity = 5, Description = "ABC", Genre = Genre.Thriller };

            // Act
            var actualBook = _repository.GetBookById(1);

            // Assert
            Assert.Equal(expectedBook.Id, actualBook.Id);
        }
        #endregion

        #region Test Add Book
        [Fact]
        public void AddBook_ReturnIfAddedSuccessfully()
        {
            //Arrange
            Book newBook = new Book() { Id = 30, Title = "BCD", Author = "BCD", PublicationYear = 2012, Quantity = 15, Genre = Genre.Fantasy };
            var expected = 26;

            //Act
            _repository.LoadData();
            _repository.Add(newBook);
            IEnumerable<Book> list = _repository.GetAll();
            int actual = list.Count();

            //Assert
            Assert.Equal(expected, actual);
        }
        #endregion

        #region Test Update
        [Fact]
        public void UpdateBook_ReturnIfUpdatedSuccessfully()
        {
            //Arrange
            //"Id": 20,
            //"Title": "Hot Rods to Hell",
            //"Author": "Clare Sacker",
            //"PublicationYear": 1919,
            //"Quantity": 11,
            //"Description": "Donec diam neque, vestibulum eget, vulputate ut, ultrices vel, augue.",
            //"Genre": "Fantasy",
            var expected = new Book { Id = 20, Title = "Hot Rods to Hell", Author = "TruongAnhKiet", PublicationYear = 2020, Quantity = 15, Description = "rathay", Genre = Genre.Romance };

            //Act
            _bookService.Update(expected);

            var actual = _repository.GetBookById(expected.Id);
            //Assert
            Assert.NotEqual(expected.Author, actual.Author);
        }
        #endregion

        #region Test Delete
        [Fact]
        public void DeleteBook_ReturnTrueNumberOfBook()
        {
            var expected = 25;
            _repository.LoadData();

            var target = _repository.GetBookById(15);

            _repository.Remove(target);
            var actual = _repository.GetAll().Count();

            Assert.Equal(expected, actual);
        }
        #endregion
    }
}