﻿public class Loan
{
    #region Fields
    private int _id;
    private List<Book> _borrowedBook;
    private List<Reader> _borrower;
    private DateTime _borrowedDate;
    private DateTime _dueDate;
    #endregion

    #region Properties
    public int Id
    {
        get => _id;
        set => _id = value;
    }

    public List<Book> BorrowedBook
    {
        get => _borrowedBook;
        set => _borrowedBook = value;
    }

    public List<Reader> Borrower
    {
        get => _borrower;
        set => _borrower = value;
    }

    public DateTime BorrowDate
    {
        get => _borrowedDate;
        set => _borrowedDate = value;
    }

    public DateTime DueDate
    {
        get => _dueDate;
        set => _dueDate = value;
    }
    #endregion
}
