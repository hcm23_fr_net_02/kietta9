﻿public class Reader
{
    #region Fields
    private int _id;
    private string _name;
    private string _address;
    private string _phoneNumber;
    #endregion

    #region Properties
    public int Id
    {
        get => _id;
        set => _id = value;
    }

    public string Name
    {
        get => _name;
        set
        {
            if (string.IsNullOrEmpty(value) || value.Trim() == "")
            {
                throw new Exception("Name must not be empty or contain only whitespace characters.");
            }
            _name = value;
        }
    }

    public string Address
    {
        get => _address;
        set
        {
            if (string.IsNullOrEmpty(value) || value.Trim() == "")
            {
                throw new Exception("Address must not be empty or contain only whitespace characters.");
            }
            _address = value;
        }
    }

    public string PhoneNumber
    {
        get => _phoneNumber;
        set
        {
            if (string.IsNullOrEmpty(value) || value.Trim() == "")
            {
                throw new Exception("Phone number must not be empty or contain only whitespace characters.");
            }
            _phoneNumber = value;
        }
    }
    #endregion
}
