﻿public class Review
{
    #region Fields
    private int _id;
    private string _title;
    private string _content;
    private int _rating;
    private int _customerId;
    private DateTime _reviewdDate;
    #endregion

    #region Properties
    public int Id
    {
        get => _id;
        set => _id = value;
    }

    public string Title
    {
        get => _title;
        set
        {
            if (string.IsNullOrEmpty(value))
            {
                throw new Exception("Tittle not empty or represent a whitespace,...");
            }
            _title = value;
        }
    }

    public string Content
    {
        get => _content;
        set
        {
            if (string.IsNullOrEmpty(value))
            {
                throw new Exception("Content not empty or represent a whitespace,...");
            }
            _content = value;
        }
    }

    public int CustomerId
    {
        get => _customerId;
        set => _customerId = value;
    }

    public int Rating
    {
        get => _rating;
        set
        {
            if (value < 1 || value > 5)
            {
                throw new Exception("Rating must be between 1 to 5");
            }
            _rating = value;
        }
    }

    public DateTime ReviewDate
    {
        get => _reviewdDate;
        set => _reviewdDate = value;
    }
    #endregion
}
