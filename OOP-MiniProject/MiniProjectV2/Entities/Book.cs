﻿using System.Text;

public class Book
{
    #region Fields
    private int _id;
    private string _title;
    private string _author;
    private int _publicationYear;
    private int _quantity;
    private string _description;
    private Genre _genre;
    public List<Review>? Reviews { get; set; }

    public Book()
    {
        Reviews = new List<Review>();
    }
    #endregion

    #region Properties
    public int Id
    {
        get => _id;
        set => _id = value;
    }

    public string Title
    {
        get => _title;
        set
        {
            if (string.IsNullOrEmpty(value) || value.Trim() == "")
            {
                throw new Exception("Title must not be empty or contain only whitespace characters.");
            }
            _title = value;
        }
    }

    public string Author
    {
        get => _author;
        set
        {
            if (string.IsNullOrEmpty(value) || value.Trim() == "")
            {
                throw new Exception("Author must not be empty or contain only whitespace characters.");
            }
            _author = value;
        }
    }

    public int PublicationYear
    {
        get => _publicationYear;
        set
        {
            if (value <= 0)
            {
                throw new Exception("Publication Year must be greater than 0");
            }
            _publicationYear = value;
        }
    }

    public int Quantity
    {
        get => _quantity;
        set
        {
            if (value <= 0)
            {
                throw new Exception("Quantity must be greater than 0");
            }
            _quantity = value;
        }
    }

    public string Description
    {
        get => _description;
        set => _description = value;
        //{
        //    if (string.IsNullOrEmpty(value) || value.Trim() == "")
        //    {
        //        throw new Exception("Description must not be empty or contain only whitespace characters.");
        //    }
        //    _description = value;
        //}
    }

    public Genre Genre
    {
        get => _genre;
        set => _genre = value;
    }

    #endregion

    #region Public Method
    public override string ToString()
    {
        StringBuilder sb = new StringBuilder();
        sb.AppendLine($"Book Id: {Id}")
          .AppendLine($"Title: {Title}")
          .AppendLine($"Author: {Author}")
          .AppendLine($"Publication Year: {PublicationYear}")
          .AppendLine($"Quantity: {Quantity}")
          .AppendLine($"Description: {Description}")
          .AppendLine($"Genre: {Genre}");
        return sb.ToString();
    }
    #endregion
}
