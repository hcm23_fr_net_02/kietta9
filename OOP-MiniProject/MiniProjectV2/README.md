# Ứng Dụng Quản Lý Thư Viện Sách

## Mô Tả

Ứng dụng Quản Lý Thư Viện Sách là một ứng dụng console cho phép bạn quản lý danh sách sách, thông tin mượn sách và quản lý thông tin người đọc trong một thư viện. Dưới đây là các chức năng chính của ứng dụng:

1. **Xem Danh Sách Sách Trong Thư Viện:**
    - Khi chọn tính năng này, bạn có thể xem danh sách các cuốn sách trong thư viện theo từng trang.
    - Sử dụng các phím sau:
        - "N" để xem trang tiếp theo.
        - "P" để quay lại trang trước.
        - "B" để quay lại menu chính.
    - Bạn có thể nhập ID của sách để xem thông tin chi tiết, chỉnh sửa, xóa, thêm đánh giá, xem danh sách đánh giá hoặc mượn sách.

2. **Thêm Sách Mới Vào Thư Viện:**
    - Bạn có thể thêm thông tin sách mới vào cơ sở dữ liệu bằng cách cung cấp các thông tin như:
        - Tựa đề (Title).
        - Tác giả (Author).
        - Năm xuất bản (Publication Year).
        - Số lượng (Quantity).
        - Thông tin khác (ví dụ: thể loại, mô tả).

3. **Lọc Sách Theo Tiêu Chí:**
    - Bạn có thể tùy chọn lọc danh sách sách theo nhiều tiêu chí như tên sách, tác giả, năm xuất bản và thể loại.

4. **Thông Tin Sách Mượn:**
    - Ứng dụng hiển thị danh sách các lượt mượn sách theo từng trang.
    - Sử dụng các phím sau:
        - "N" để xem trang tiếp theo.
        - "P" để quay lại trang trước.
        - "B" để quay lại menu chính.
    - Bạn có thể tìm kiếm lượt mượn bằng tên người mượn hoặc tên cuốn sách và nhập ID để trả sách.

5. **Quản Lý Thông Tin Người Đọc:**
    - Ứng dụng cung cấp giao diện để quản lý thông tin người đọc, bao gồm xem danh sách, chỉnh sửa thông tin và xóa người đọc.

6. **Thoát Ứng Dụng:**
    - Khi bạn muốn kết thúc ứng dụng, bạn có thể chọn tính năng này.
    - Ứng dụng sử dụng lưu trữ dữ liệu tạm thời trong bộ nhớ để giúp bạn phát triển và kiểm thử nhanh chóng.
    - Dữ liệu giả được tạo bằng Mockaroo để làm dữ liệu mẫu.

## Lưu Ý

- Ứng dụng sử dụng lưu trữ dữ liệu tạm thời trong bộ nhớ, do đó, dữ liệu không được lưu trên cơ sở dữ liệu thực.
- Dữ liệu giả được tạo bằng Mockaroo để làm dữ liệu mẫu.

## Tác Giả

- Tên: Trương Anh Kiệt
- Liên hệ: truonganhkietdev@gmail.com

## Giấy Phép

Ứng dụng này được phân phối dưới giấy phép MIT. Xem tệp LICENSE để biết chi tiết.

---

