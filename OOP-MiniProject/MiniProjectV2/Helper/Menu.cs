﻿public class Menu
{
    public static void MainMenu()
    {
        Console.WriteLine("*** LibraryManagement ***");
        Console.WriteLine("==========================");
        Console.WriteLine("|1. View all books       |");
        Console.WriteLine("|2. Add new book         |");
        Console.WriteLine("|3. Filter book          |");
        Console.WriteLine("|4. Loan information     |");
        Console.WriteLine("|5. Reader management    |");
        Console.WriteLine("|6. Exit                 |");
        Console.WriteLine("==========================");
        Console.Write("Your choice: ");
    }

    public static void BookSubMenu()
    {
        Console.WriteLine("==========================");
        Console.WriteLine("|1. Update book          |");
        Console.WriteLine("|2. Delete book          |");
        Console.WriteLine("|3. Add review           |");
        Console.WriteLine("|4. View all reviews     |");
        Console.WriteLine("|5. Make a loan          |");
        Console.WriteLine("|6. Back to MainMenu     |");
        Console.WriteLine("==========================");
        Console.Write("Your choice: ");
    }

    public static void ReaderSubMenu()
    {
        Console.WriteLine("===========================");
        Console.WriteLine("|1. View all readers      |");
        Console.WriteLine("|2. Add reader            |");
        Console.WriteLine("|3. Update reader         |");
        Console.WriteLine("|4. Delete reader         |");
        Console.WriteLine("|5. Back to MainMenu      |");
        Console.WriteLine("===========================");
        Console.Write("Your choice: ");
    }

    public static void LoanSubMenu()
    {
        Console.WriteLine("==================================================");
        Console.WriteLine("|1. View all loans                               |");
        Console.WriteLine("|2. Find loan (Book's title/Borrower's name)     |");
        Console.WriteLine("|3. Return book                                  |");
        Console.WriteLine("|4. Back to Main Menu                            |");
        Console.WriteLine("==================================================");
        Console.Write("Your choice: ");
    }
}

