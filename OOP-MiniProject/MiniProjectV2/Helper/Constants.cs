﻿public class Constants

{
    //CATCH EXCEPTION
    public const string EXCEPTION = "Error: ";
    public const string INVALID_ID = "Invalid ID!";

    //PROMT BOOK FIELDS
    public const string BOOK_ID = "Id: ";
    public const string BOOK_TITLE = "Title: ";
    public const string BOOK_AUTHOR = "Author: ";
    public const string BOOK_PUBLICATION = "Publication Year: ";
    public const string BOOK_QUANTITY = "Quantity: ";
    public const string BOOK_DESCRIPTION = "Description: ";
    public const string BOOK_GENRE = "Genre: ";
    public const string BOOK_REVIEWS = "Reviews: ";

    //INPUT BOOK'S FIELS REQUIREMENTS
    public const string ENTER_ID = "Enter Id: ";
    public const string ENTER_TITLE = "Enter Title: ";
    public const string ENTER_AUTHOR = "Enter Author: ";
    public const string ENTER_PUBYEAR = "Enter Pulication Year: ";
    public const string ENTER_QUANTITY = "Enter Quantity: ";
    public const string ENTER_DESCRIPTION = "Enter Description: ";
    public const string ENTER_GENRE = "Select a Genre: ";
    public const string ADDED_NOTI = "Book added successfully!";

    //GENRE TYPE
    public const string INVALID_GENRE = "Invalid Genre choice. Setting to default.";

    //UPDATE BOOK
    public const string UPDATE_ID_REQUIRE = "Enter Book's Id to update: ";
    public const string UPDATED_NOTI = "Book updated successfully!";

    //DELETE BOOK
    public const string DELETE_ID_REQUIRE = "Enter Book's Id to delete: ";
    public const string DELETED_NOTI = "Book deleted successfully!";









}
