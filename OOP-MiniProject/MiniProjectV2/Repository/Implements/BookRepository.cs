﻿public class BookRepository : GenericRepository<Book>, IBookRepository
{
    public BookRepository(string filePath) : base(filePath)
    {
    }

    public IEnumerable<Book> FilterBooks(Func<Book, bool> filterCondition)
    {
        var filter = _entities.Where(x => filterCondition(x));
        if (filter is null)
        {
            throw new Exception();
        }
        return filter;
    }

    public Book GetBookById(int id)
    {
        return _entities.FirstOrDefault(e => e.Id == id);
    }


    public IEnumerable<Book> Paging(int pageIndex, int pageSize)
    {
        return _entities.Skip(((pageIndex - 1) * pageSize)).Take(pageSize);
    }

}


