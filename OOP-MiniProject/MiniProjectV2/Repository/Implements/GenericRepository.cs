﻿using Newtonsoft.Json;

public class GenericRepository<T> : IGenericRepository<T> where T : class
{
    private readonly string filePath;
    protected List<T> _entities = new List<T>();

    public GenericRepository(string filePath)
    {
        this.filePath = filePath;
    }

    public void LoadData()
    {
        if (File.Exists(filePath))
        {
            string jsonData = File.ReadAllText(filePath);
            _entities = JsonConvert.DeserializeObject<List<T>>(jsonData);
        }
        else
        {
            Console.WriteLine("Cannot find file path");
        }
    }

    public void SaveData()
    {
        string jsonData = JsonConvert.SerializeObject(_entities);
        File.WriteAllText(filePath, jsonData);
    }

    public void Add(T entity)
    {
        _entities.Add(entity);
        SaveData();
    }

    public IEnumerable<T> Find(Func<T, bool> predicate) => _entities.Where(predicate);

    public IEnumerable<T> GetAll() => _entities;

    public void Remove(T entity)
    {
        _entities.Remove(entity);
        SaveData();
    }
}