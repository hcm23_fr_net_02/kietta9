﻿public class ReaderRepository : GenericRepository<Reader>, IReaderRepository
{
    public ReaderRepository(string filePath) : base(filePath)
    {
    }

    public Reader GetReader(int id)
    {
        var reader = _entities.FirstOrDefault(r => r.Id == id);
        if (reader is null)
        {
            throw new Exception();
        }
        return reader;
    }
}
