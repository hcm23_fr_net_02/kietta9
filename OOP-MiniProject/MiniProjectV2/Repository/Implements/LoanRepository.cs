﻿public class LoanRepository : GenericRepository<Loan>, ILoanRepository
{
    public LoanRepository(string filePath) : base(filePath)
    {
    }

    public Loan GetLoanById(int id)
    {
        var loan = _entities.FirstOrDefault(e => e.Id == id);
        if (loan is null)
        {
            throw new Exception();
        }
        return loan;
    }
}

