﻿public interface IGenericRepository<T> where T : class
{
    IEnumerable<T> GetAll();
    IEnumerable<T> Find(Func<T, bool> predicate);
    void Add(T entity);
    void Remove(T entity);
    void LoadData();
    void SaveData();
}

