﻿public interface IReaderRepository : IGenericRepository<Reader>
{
    Reader GetReader(int id); 
}

