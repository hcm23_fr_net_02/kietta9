﻿public interface ILoanRepository : IGenericRepository<Loan>
{
    Loan GetLoanById(int id);
}

