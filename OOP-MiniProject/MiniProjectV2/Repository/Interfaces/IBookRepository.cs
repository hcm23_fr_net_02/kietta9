﻿public interface IBookRepository : IGenericRepository<Book>
{
    Book GetBookById(int id);
    IEnumerable<Book> FilterBooks(Func<Book, bool> filterCondition);
    IEnumerable<Book> Paging(int pageIndex = 1, int pageSize = 5);
}

