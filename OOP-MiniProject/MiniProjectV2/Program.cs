﻿IBookRepository bookRepository = new BookRepository("books.json");
IReaderRepository readerRepository = new ReaderRepository("readers.json");
ILoanRepository loanRepository = new LoanRepository("loans.json");

BookService bookService = new BookService(bookRepository);
ReaderService readerService = new ReaderService(readerRepository);
LoanService loanService = new LoanService(bookRepository, readerRepository, loanRepository);

bookRepository.LoadData();
readerRepository.LoadData();

int choice = 0;
try
{
    Console.OutputEncoding = System.Text.Encoding.UTF8;
    do
    {
        Menu.MainMenu();
        choice = Convert.ToInt32(Console.ReadLine());

        switch (choice)
        {
            case 1:
                bookService.DisplayBooks();
                Menu.BookSubMenu();
                int subMenuChoice = Convert.ToInt32(Console.ReadLine());
                Book book = new Book();


                switch (subMenuChoice)
                {

                    case 1:
                        bookService.Update(book);
                        break;
                    case 2:
                        bookService.Delete();
                        break;
                    case 3:
                        Console.WriteLine("Enter bookId: ");
                        int bookId = Convert.ToInt32(Console.ReadLine());
                        bookService.AddReview(bookId);
                        break;
                    case 4:
                        Console.WriteLine("Enter Book ID: ");
                        int id = Convert.ToInt32(Console.ReadLine());
                        bookService.ShowAllReviews(id);
                        break;
                    case 5:
                        Console.WriteLine("Make a loan");
                        loanService.MakeLoan();
                        break;
                    case 6:
                        break;
                    default:
                        Console.WriteLine("Invalid choice");
                        break;
                }
                break;
            case 2:
                Book newBook = new Book();
                bookService.Add(newBook);
                break;
            case 3:
                bookService.FilterBook();
                break;
            case 4:
                Menu.LoanSubMenu();
                int loanChoice = Convert.ToInt32(Console.ReadLine());
                switch (loanChoice)
                {
                    case 1:
                        loanService.GetAll();
                        break;
                    case 2:
                        Console.WriteLine("Enter Book's title or Borrower's name: ");
                        string loanString = Convert.ToString(Console.ReadLine());
                        loanService.GetLoan(loanString);
                        break;
                    case 3:
                        Console.WriteLine("Enter Loan ID: ");
                        int loanId = Convert.ToInt32(Console.ReadLine());
                        loanService.ReturnBook(loanId);
                        break;
                    case 4:
                        //Back to Main Menu
                        break;
                    default:
                        Console.WriteLine("Invalid choice");
                        break;
                }

                break;
            case 5:
                Menu.ReaderSubMenu();
                int readerChoice = Convert.ToInt32(Console.ReadLine());

                switch (readerChoice)
                {
                    case 1:
                        readerService.GetAll();
                        break;
                    case 2:
                        Reader reader = new Reader();
                        readerService.Add(reader);
                        break;
                    case 3:
                        Reader updateReader = new Reader();
                        Console.WriteLine("Enter ID of reader to update: ");
                        int id = Convert.ToInt32(Console.ReadLine());

                        Console.Write("Enter Name: ");
                        updateReader.Name = Console.ReadLine();

                        Console.Write("Enter Address: ");
                        updateReader.Address = Console.ReadLine();

                        Console.Write("Enter PhoneAddress: ");
                        updateReader.PhoneNumber = Console.ReadLine();

                        readerService.Update(id, updateReader);
                        break;
                    case 4:
                        Console.Write("Enter the ID of the reader to delete: ");
                        int deltedId = Convert.ToInt32(Console.ReadLine());
                        readerService.Delete(deltedId);
                        break;
                    case 5:
                        //Back to Main Menu
                        break;
                    default:
                        break;
                }
                break;
            case 6:
                break;
            default:
                Console.WriteLine("Invalid choice");
                break;
        }
    } while (choice != 6);
}
catch (Exception ex)
{
    Console.WriteLine($"Error: {ex.Message}");
}
