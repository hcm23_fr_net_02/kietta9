﻿public class LoanService : ILoanService
{
    private readonly IBookRepository bookRepository;
    private readonly IReaderRepository readerRepository;
    private readonly ILoanRepository loanRepository;

    public LoanService(IBookRepository bookRepository, IReaderRepository readerRepository, ILoanRepository loanRepository)
    {
        this.bookRepository = bookRepository;
        this.readerRepository = readerRepository;
        this.loanRepository = loanRepository;
    }

    public IEnumerable<Loan> GetAll()
    {
        var loans = loanRepository.GetAll();
        foreach (var item in loans)
        {
            Console.WriteLine($"Loan ID: {item.Id}");
            Console.WriteLine("Books Borrowed:");
            foreach (var book in item.BorrowedBook)
            {
                Console.WriteLine($"  Book ID: {book.Id}");
                Console.WriteLine($"  Title: {book.Title}");
            }
            Console.WriteLine("Readers:");
            foreach (var reader in item.Borrower)
            {
                Console.WriteLine($"  Reader ID: {reader.Id}");
                Console.WriteLine($"  Name: {reader.Name}");
            }
            Console.WriteLine($"Borrow Date: {item.BorrowDate:yyyy-MM-dd}");
            Console.WriteLine($"Due Date: {item.DueDate:yyyy-MM-dd}");
        }
        return loans;
    }

    public Loan GetLoan(string input)
    {
        // Implement searching loans by criteria (e.g., borrower name or book title)
        // and return the matching loan.
        var loans = loanRepository.GetAll();
        var matchingLoans = loans.Where(loan =>
            loan.Borrower.Any(reader => reader.Name.ToLower().Contains(input.ToLower())) ||
            loan.BorrowedBook.Any(book => book.Title.ToLower().Contains(input.ToLower())));

        if (matchingLoans.Any())
        {
            foreach (var loan in matchingLoans)
            {
                Console.WriteLine($"Loan ID: {loan.Id}");
                Console.WriteLine($"Borrowed Books: {string.Join(", ", loan.BorrowedBook.Select(b => b.Title))}");
                Console.WriteLine($"Borrower: {loan.Borrower.First().Name}");
                Console.WriteLine($"Borrow Date: {loan.BorrowDate:yyyy-MM-dd}");
                Console.WriteLine($"Due Date: {loan.DueDate:yyyy-MM-dd}");
            }
            return matchingLoans.First();
        }
        else
        {
            Console.WriteLine("No matching loans found.");
            return null;
        }
    }

    public void MakeLoan()
    {
        try
        {
            Console.WriteLine("Enter loan details:");
            Console.Write("Enter the reader's ID: ");
            int readerId = Convert.ToInt32(Console.ReadLine());

            Console.Write("Enter the book's ID: ");
            int bookId = Convert.ToInt32(Console.ReadLine());

            Console.Write("Enter the loan date (yyyy-MM-dd): ");
            DateTime loanDate = DateTime.Parse(Console.ReadLine());

            Console.Write("Enter the return date (yyyy-MM-dd): ");
            DateTime returnDate = DateTime.Parse(Console.ReadLine());

            Book book = bookRepository.GetBookById(bookId);
            Reader reader = readerRepository.GetReader(readerId);

            // Step 3: Check if the book is available
            if (book.Quantity <= 0)
            {
                Console.WriteLine("The selected book is not available for loan.");
                return;
            }

            // Step 4: Create a new loan record
            Loan loan = new Loan
            {
                BorrowedBook = new List<Book> { book },
                Borrower = new List<Reader> { reader },
                BorrowDate = loanDate,
                DueDate = returnDate
            };

            // Update book quantity and add the loan to the repository
            book.Quantity--;
            loanRepository.Add(loan);

            Console.WriteLine("Loan created successfully.");
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Error: {ex.Message}");
        }
    }

    public void ReturnBook(int loanId)
    {
        Loan loan = loanRepository.GetLoanById(loanId);

        if (loan == null)
        {
            Console.WriteLine("Loan not found.");
            return;
        }

        // Check if the due date has passed
        if (loan.DueDate < DateTime.Now)
        {
            Console.WriteLine("The book is overdue. Please contact the library.");
            return;
        }

        // Mark the book as returned and update book quantity
        Book book = loan.BorrowedBook.First();
        book.Quantity++;
        loanRepository.Remove(loan);

        Console.WriteLine("Book returned successfully.");
    }
}

