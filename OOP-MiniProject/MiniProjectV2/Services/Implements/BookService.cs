﻿public class BookService : IBookService
{
    private readonly IBookRepository _bookRepository;

    public BookService(IBookRepository bookRepository)
    {
        _bookRepository = bookRepository;
    }

    public IEnumerable<Book> GetAll(int pageIndex, int pageSize)
    {
        var books = _bookRepository.Paging(pageIndex, pageSize);
        foreach (var book in books)
        {
            Console.WriteLine(Constants.BOOK_ID + book.Id);
            Console.WriteLine(Constants.BOOK_TITLE + book.Title);
            Console.WriteLine(Constants.BOOK_AUTHOR + book.Author);
            Console.WriteLine(Constants.BOOK_PUBLICATION + book.PublicationYear);
            Console.WriteLine(Constants.BOOK_QUANTITY + book.Quantity);
            Console.WriteLine(Constants.BOOK_DESCRIPTION + book.Description);
            Console.WriteLine(Constants.BOOK_GENRE + book.Genre);

            if (book.Reviews != null)
            {

                Console.WriteLine(Constants.BOOK_REVIEWS);

                foreach (var review in book.Reviews)
                {
                    Console.WriteLine("  Review ID: " + review.Id);
                    Console.WriteLine("  Title: " + review.Title);
                    Console.WriteLine("  Content: " + review.Content);
                    Console.WriteLine("  Rating: " + review.Rating);
                    Console.WriteLine("  Review Date: " + review.ReviewDate);
                    Console.WriteLine("  Customer ID: " + review.CustomerId);
                    Console.WriteLine();
                }
            }
            else
            {
                Console.WriteLine("Null Reviews");
            }
            Console.WriteLine();
        }

        return books;
    }

    public bool GetBookById(int id)
    {
        var book = _bookRepository.GetBookById(id);
        if (book == null)
        {
            return false;
        }
        return true;
    }

    public void Add(Book book)
    {
        try
        {
            Console.Write(Constants.ENTER_ID);
            book.Id = Convert.ToInt32(Console.ReadLine());

            Console.Write(Constants.ENTER_TITLE);
            book.Title = Console.ReadLine();

            Console.Write(Constants.ENTER_AUTHOR);
            book.Author = Console.ReadLine();

            Console.Write(Constants.ENTER_PUBYEAR);
            book.PublicationYear = Convert.ToInt32(Console.ReadLine());

            Console.Write(Constants.ENTER_QUANTITY);
            book.Quantity = Convert.ToInt32(Console.ReadLine());

            Console.Write(Constants.ENTER_DESCRIPTION);
            book.Description = Console.ReadLine();

            Console.WriteLine(Constants.ENTER_GENRE);
            foreach (Genre genre in Enum.GetValues(typeof(Genre)))
            {
                Console.WriteLine($"{(int)genre}: {genre}");
            }

            int genreChoice = Convert.ToInt32(Console.ReadLine());
            if (Enum.IsDefined(typeof(Genre), genreChoice))
            {
                book.Genre = (Genre)genreChoice;
            }
            else
            {
                Console.WriteLine(Constants.INVALID_GENRE);
                book.Genre = Genre.Fantasy;
            }

            _bookRepository.Add(book);
            Console.WriteLine(Constants.ADDED_NOTI);
        }
        catch (Exception ex)
        {
            Console.WriteLine(Constants.EXCEPTION + ex.Message); ;
        }
    }

    public void Update(Book book)
    {
        try
        {
            Console.Write(Constants.UPDATE_ID_REQUIRE);
            int id = Convert.ToInt32(Console.ReadLine());

            var oldBook = _bookRepository.GetBookById(id);

            if (oldBook != null)
            {
                Console.Write(Constants.ENTER_TITLE);
                book.Title = Console.ReadLine();

                Console.Write(Constants.ENTER_AUTHOR);
                book.Author = Console.ReadLine();

                Console.Write(Constants.ENTER_PUBYEAR);
                book.PublicationYear = Convert.ToInt32(Console.ReadLine());

                Console.Write(Constants.ENTER_QUANTITY);
                book.Quantity = Convert.ToInt32(Console.ReadLine());

                Console.Write(Constants.ENTER_DESCRIPTION);
                book.Description = Console.ReadLine();

                Console.WriteLine(Constants.ENTER_GENRE);
                foreach (Genre genre in Enum.GetValues(typeof(Genre)))
                {
                    Console.WriteLine($"{(int)genre}: {genre}");
                }

                int genreChoice = Convert.ToInt32(Console.ReadLine());
                if (Enum.IsDefined(typeof(Genre), genreChoice))
                {
                    book.Genre = (Genre)genreChoice;
                }
                else
                {
                    Console.WriteLine(Constants.INVALID_GENRE);
                    book.Genre = Genre.Fantasy;
                }

                // Update the properties of the old book with the new book's properties
                oldBook.Title = book.Title;
                oldBook.Author = book.Author;
                oldBook.PublicationYear = book.PublicationYear;
                oldBook.Quantity = book.Quantity;
                oldBook.Description = book.Description;
                oldBook.Genre = book.Genre;

                // Save the updated book back to the repository
                Console.WriteLine(Constants.UPDATED_NOTI);
            }
            else
            {
                Console.WriteLine(Constants.INVALID_ID);
            }
        }
        catch (Exception ex)
        {
            Console.WriteLine(Constants.EXCEPTION + ex.Message);
        }
    }

    public void Delete()
    {
        try
        {
            Console.Write(Constants.DELETE_ID_REQUIRE);
            int id = Convert.ToInt32(Console.ReadLine());
            var deleteBook = _bookRepository.GetBookById(id);

            if (deleteBook != null)
            {
                _bookRepository.Remove(deleteBook);
                Console.WriteLine(Constants.DELETED_NOTI);
            }
            else
            {
                Console.WriteLine(Constants.INVALID_ID);
            }
        }
        catch (Exception ex)
        {
            Console.WriteLine(Constants.EXCEPTION + ex.Message); ;
        }
    }

    public void AddReview(int bookId)
    {
        try
        {
            var book = _bookRepository.GetBookById(bookId);

            if (book != null)
            {
                Review review = new Review();

                Console.Write("Enter the title of your review: ");
                review.Title = Console.ReadLine();

                Console.Write("Enter the review content: ");
                review.Content = Console.ReadLine();

                Console.Write("Enter your Customer ID: ");
                review.CustomerId = Convert.ToInt32(Console.ReadLine());

                Console.Write("Enter your rating (1 to 5): ");
                int rating = Convert.ToInt32(Console.ReadLine());
                if (rating < 1 || rating > 5)
                {
                    throw new Exception("Rating must be between 1 to 5.");
                }
                review.Rating = rating;

                review.ReviewDate = DateTime.Now;


                if (book.Reviews == null)
                {
                    book.Reviews = new List<Review>();
                    book.Reviews.Add(review);

                    Console.WriteLine("Review added successfully.");
                }


            }
            else
            {
                Console.WriteLine($"Book with ID {bookId} not found.");
            }
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Error: {ex.Message}");
        }
    }

    public void ShowAllReviews(int bookId)
    {
        var book = _bookRepository.GetBookById(bookId);

        if (book != null && book.Reviews.Any())
        {
            Console.WriteLine($"Reviews for the book '{book.Title}':");

            foreach (var review in book.Reviews)
            {
                Console.WriteLine($"Review ID: {review.Id}");
                Console.WriteLine($"Title: {review.Title}");
                Console.WriteLine($"Content: {review.Content}");
                Console.WriteLine($"Rating: {review.Rating}");
                Console.WriteLine($"Review Date: {review.ReviewDate}");
                Console.WriteLine($"Customer ID: {review.CustomerId}");
                Console.WriteLine();
            }
        }
        else
        {
            Console.WriteLine($"No reviews found for the book with ID {bookId}.");
        }
    }

    public void DisplayBooks()
    {
        int pageIndex = 1;
        const int pageSize = 5; // Số lượng sách tối đa trên mỗi trang


        while (true)
        {
            var books = GetAll(pageIndex, pageSize).ToList();

            if (!books.Any())
            {
                Console.WriteLine("Không có sách nào để hiển thị.");
                return;
            }
            
            Console.Clear();

            // Hiển thị sách

            foreach (var book in books)
            {
                Console.WriteLine($"\n|Id: {book.Id}, Title: {book.Title}, Author: {book.Author}, Description: {book.Description}, Quantity: {book.Quantity}, Publication Year: {book.PublicationYear}, Genre: {book.Genre}");
            }
            
            Console.WriteLine("\n[N] for next page \t[P] for previous page \t[B] back to Main Menu ");

            var key = Console.ReadKey(true).Key;
            switch (key)
            {
                case ConsoleKey.N:
                    pageIndex++;
                    break;
                case ConsoleKey.P:
                    if (pageIndex > 1)
                    {
                        pageIndex--;
                    }
                    break;
                case ConsoleKey.B:
                    return;
                default:
                    Console.WriteLine("Phím không hợp lệ. Vui lòng thử lại.");
                    break;
            }
        }
    }


    public void FilterBook()
    {
        Console.WriteLine("Filter by:");
        Console.WriteLine("1. Title");
        Console.WriteLine("2. Author");
        Console.WriteLine("3. Publication Year");
        Console.WriteLine("4. Genre");
        Console.Write("Choose an option (1/2/3/4): ");
        string option = Console.ReadLine();

        switch (option)
        {
            case "1":
                FilterByTitle();
                break;
            case "2":
                FilterByAuthor();
                break;
            case "3":
                FilterByPublicationYear();
                break;
            case "4":
                FilterByGenre();
                break;
            default:
                Console.WriteLine("Invalid option.");
                break;
        }
    }

    public void FilterByTitle()
    {
        Console.Write("Input title to filter: ");
        var filterTitle = Console.ReadLine();

        var filteredBooks = _bookRepository.FilterBooks(book => book.Title.Contains(filterTitle));

        DisplayFilteredBooks(filteredBooks);
    }

    public void FilterByAuthor()
    {
        Console.Write("Input author to filter: ");
        var filterAuthor = Console.ReadLine();

        var filteredBooks = _bookRepository.FilterBooks(book => book.Author.Contains(filterAuthor));

        DisplayFilteredBooks(filteredBooks);
    }

    public void FilterByPublicationYear()
    {
        Console.Write("Input publication year to filter: ");
        if (int.TryParse(Console.ReadLine(), out int filterYear))
        {
            var filteredBooks = _bookRepository.FilterBooks(book => book.PublicationYear == filterYear);
            DisplayFilteredBooks(filteredBooks);
        }
        else
        {
            Console.WriteLine("Invalid input for publication year.");
        }
    }

    public void FilterByGenre()
    {
        Console.Write("Input genre to filter:");
        foreach (Genre genre in Enum.GetValues(typeof(Genre)))
        {
            Console.WriteLine($"{(int)genre}: {genre}");
        }
        if (int.TryParse(Console.ReadLine(), out int genreChoice) && Enum.IsDefined(typeof(Genre), genreChoice))
        {
            var filterGenre = (Genre)genreChoice;
            var filteredBooks = _bookRepository.FilterBooks(book => book.Genre == filterGenre);
            DisplayFilteredBooks(filteredBooks);
        }
        else
        {
            Console.WriteLine("Invalid input for genre.");
        }
    }

    public void DisplayFilteredBooks(IEnumerable<Book> books)
    {
        Console.WriteLine("Filtered Books:");
        foreach (var book in books)
        {
            Console.WriteLine($"Title: {book.Title}");
            Console.WriteLine($"Author: {book.Author}");
            Console.WriteLine($"Publication Year: {book.PublicationYear}");
            Console.WriteLine($"Quantity: {book.Quantity}");
            Console.WriteLine($"Description: {book.Description}");
            Console.WriteLine($"Genre: {book.Genre}");
            Console.WriteLine();
        }
    }
}
