﻿public class ReaderService : IReaderService
{
    private readonly IReaderRepository readerRepository;

    public ReaderService(IReaderRepository readerRepository)
    {
        this.readerRepository = readerRepository;
    }

    public IEnumerable<Reader> GetAll()
    {
        var readers = readerRepository.GetAll();
        foreach (var item in readers)
        {
            Console.WriteLine($"ID: {item.Id}");
            Console.WriteLine($"Name: {item.Name}");
            Console.WriteLine($"Address: {item.Address}");
            Console.WriteLine($"PhoneNumber: {item.PhoneNumber}");
            Console.WriteLine();
        }

        return readers;
    }

    public bool GetReaderById(int id)
    {
        var reader = readerRepository.GetReader(id);
        if (reader == null)
        {
            return false;
        }

        return true;
    }

    public void Add(Reader reader)
    {
        try
        {
            Console.Write("Enter Id: ");
            reader.Id = Convert.ToInt32(Console.ReadLine());

            Console.Write("Enter Name: ");
            reader.Name = Console.ReadLine();

            Console.Write("Enter Address: ");
            reader.Address = Console.ReadLine();

            Console.Write("Enter PhoneNumber: ");
            reader.PhoneNumber = Console.ReadLine();

            readerRepository.Add(reader);

            Console.WriteLine("Added reader successfully");
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Error: {ex.Message}"); ;
        }
    }

    public void Delete(int id)
    {
        try
        { 

            var deleteReader = readerRepository.GetReader(id);

            if (deleteReader != null)
            {
                readerRepository.Remove(deleteReader);
            }
            else
            {
                Console.WriteLine("Invalid ID");
            }
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Error: {ex.Message}"); ;
        }
    }

    public void Update(int id, Reader reader)
    {
        try
        {


            var oldReader = readerRepository.GetReader(id);

            if (oldReader != null)
            {


                oldReader.Name = reader.Name;
                oldReader.Address = reader.Address;
                oldReader.PhoneNumber = reader.PhoneNumber;

                Console.WriteLine("Reader updated successfully.");
            }
            else
            {
                Console.WriteLine("Invalid ID");
            }
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Error: {ex.Message}"); ;
        }
    }
}


