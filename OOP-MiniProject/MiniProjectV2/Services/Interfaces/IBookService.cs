﻿public interface IBookService
{
    IEnumerable<Book> GetAll(int pageIndex, int pageSize);
    bool GetBookById(int id);
    void Add(Book book);
    void Update(Book book);
    void Delete();
}

