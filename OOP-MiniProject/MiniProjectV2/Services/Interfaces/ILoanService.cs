﻿public interface ILoanService
{
    void MakeLoan();
    IEnumerable<Loan> GetAll();
    Loan GetLoan(string input);
}

