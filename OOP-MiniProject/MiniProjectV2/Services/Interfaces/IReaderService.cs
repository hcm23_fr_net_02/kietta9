﻿public interface IReaderService
{
    IEnumerable<Reader> GetAll();
    bool GetReaderById(int id);
    void Add(Reader reader);
    void Delete(int id);
    void Update(int id, Reader reader);
}

