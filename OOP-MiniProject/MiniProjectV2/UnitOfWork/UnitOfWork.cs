﻿using MiniProjectV2.UnitOfWork;

public class UnitOfWork : IUnitOfWork
{
    private readonly IBookRepository _bookRepository;
    private readonly IReaderRepository _readerRepository;

    public UnitOfWork(IBookRepository bookRepository, IReaderRepository readerRepository)
    {
        _bookRepository = bookRepository;
        _readerRepository = readerRepository;
    }

    public IBookRepository Book => _bookRepository;
    public IReaderRepository Reader => _readerRepository;

    public void Dispose()
    {
        throw new NotImplementedException();
    }
}

