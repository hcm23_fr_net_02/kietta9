﻿namespace MiniProjectV2.UnitOfWork
{
    public interface IUnitOfWork : IDisposable
    {
        IBookRepository Book { get; }
        IReaderRepository Reader { get; }
    }
}
