﻿using Newtonsoft.Json;

string jsonFilePath = "MOCK_DATA.json";

string jsonContent = File.ReadAllText(jsonFilePath);

List<Order> orders = JsonConvert.DeserializeObject<List<Order>>(jsonContent);

foreach (var item in orders)
{
    Console.WriteLine($"ID: {item.Id}");
    Console.WriteLine($"Name: {item.Name}");
    Console.WriteLine($"TotalAmount: {item.TotalAmount}");
    Console.WriteLine("-------------------");
}

//1. Tính tổng số tiền của tất cả các đơn đặt hàng. 
var sumOrder = orders.Sum(x => x.TotalAmount);
Console.WriteLine($"Sum of all orders: {sumOrder}");

//2. Lấy ra đơn hàng có giá trị (TotalAmount) cao nhất. 
var maxTotalAmount = orders.Max(x => x.TotalAmount);
Console.WriteLine($"Max Order: {maxTotalAmount}");

//3. Lấy ra đơn hàng có giá trị thấp nhất. 
var minTotalAmount = orders.Min(x => x.TotalAmount);
Console.WriteLine($"Max Order: {minTotalAmount}");

//4. Tính giá trị trung bình của các đơn hàng. 
var avg = orders.Average(x => x.TotalAmount); 
Console.WriteLine($"Average: {avg}");

//5. Tính tổng số đơn đặt hàng của một khách hàng cụ thể. 
Console.WriteLine("Enter ID to get total number of orders: ");
int id = int.Parse(Console.ReadLine());
var customerOrders = orders.Where(x => x.Id == id);
int count = customerOrders.Count();
Console.WriteLine($"Total number of orders for customer {id}: {count}");

//6. Tính tổng số tiền của tất cả các đơn đặt hàng của một khách hàng cụ thể.
decimal totalAmountForCustomer = customerOrders.Sum(x => x.TotalAmount);
Console.WriteLine($"Total amount of orders for customer {id}: {totalAmountForCustomer}");
