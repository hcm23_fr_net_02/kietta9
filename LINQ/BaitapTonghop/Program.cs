﻿// Hãy giả định chúng ta có một danh sách các sản phẩm của một cửa hàng với các thuộc tính sau: 
// tên sản phẩm, giá, và danh mục (category). 
// Hãy tạo một danh sách các sản phẩm và thực hiện các yêu cầu sau bằng cách sử dụng Linq:

// Tạo danh sách các sản phẩm:
var products = new List<Product>
{
    new Product { Name = "Áo sơ mi", Price = 300000, Category = "Áo" },
    new Product { Name = "Quần jeans", Price = 500000, Category = "Quần" },
    new Product { Name = "Giày thể thao", Price = 800000, Category = "Giày" },
    new Product { Name = "Ví da", Price = 200000, Category = "Phụ kiện" },
    new Product { Name = "Mũ nón", Price = 100000, Category = "Phụ kiện" },
    new Product { Name = "Đầm dạ hội", Price = 1500000, Category = "Đầm" },
    // Thêm các sản phẩm khác tại đây...
};

// Tìm danh sách các sản phẩm có giá lớn hơn 500,000 đồng:
//var rangeProduct = products.Where(x => x.Price > 500000).ToList();
//foreach (var item in rangeProduct)
//{
//    Console.WriteLine($"{item.Name}");
//}


// Lấy ra tên của các sản phẩm có giá nhỏ hơn 300,000 đồng và sắp xếp theo thứ tự giảm dần của giá:
var nameProduct = products.Where(x => x.Price > 300000).OrderByDescending(x => x.Price).ToList();
foreach (var item in nameProduct)
{
    Console.WriteLine($"{item.Name}");
}



// Tạo một dictionary với danh mục sản phẩm là key và danh sách các sản phẩm trong cùng danh mục là value:
var dictionary = products.GroupBy(x => x.Category);
foreach (var item in dictionary)
{
    Console.WriteLine($"Group key {item.Key}");
    foreach (var item1 in item)
    {
        Console.WriteLine(item1);
    }
}


// Tính tổng giá tiền của các sản phẩm trong danh mục "Áo":
var sum = products.Where(x => x.Category == "Áo").Sum(x => x.Price);
Console.WriteLine(sum);


// Tìm sản phẩm có giá cao nhất:
var max = products.Max(x => x.Price);
Console.WriteLine(max);


// Tạo danh sách các sản phẩm với tên đã viết hoa:
var toupper = products.Select(x => new 
                        {
                        Name = x.Name.ToUpper(),
                        Price = x.Price,
                        Category = x.Category,
                        }).ToList();
foreach (var item in toupper)
{
    Console.WriteLine(item);
}
class Product
{
    public string Name { get; set; }
    public int Price { get; set; }
    public string Category { get; set; }
}