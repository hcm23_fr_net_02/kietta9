﻿// đọc file mock-data.json trong project để lấy dữ liệu

using Newtonsoft.Json;
using System;

string jsonFilePath = "mockdata.json";

string jsonContent = File.ReadAllText(jsonFilePath);

List<Employee> employees = JsonConvert.DeserializeObject<List<Employee>>(jsonContent);

// Access the deserialized data
//foreach (var item in employees)
//{
//    Console.WriteLine($"Id: {item.Id}");
//    Console.WriteLine($"FirstName: {item.FirstName}");
//    Console.WriteLine($"LastName: {item.LastNAme}");
//    Console.WriteLine($"LastName: {item.LastNAme}");
//    Console.WriteLine($"Email: {item.Email}");
//    Console.WriteLine($"Gender: {item.Gender}");
//    Console.WriteLine($"Country: {item.country}");
//    Console.WriteLine($"PostalCode: {item.PostalCode}");
//    Console.WriteLine($"JobTitle: {item.JobTitle}");
//    Console.WriteLine($"StartDate: {item.StartDate}");
//    Console.WriteLine($"Salary: {item.Salary}");
//    Console.WriteLine("-----------------------------");
//}


// sử dụng LinQ để lọc các dữ liệu như yêu cầu bên dưới

//Lấy ra danh sách các FirstName của tất cả các nhân viên.
var firstNames = employees.Select(x => x.FirstName);
foreach (var item in firstNames)
{
    Console.WriteLine($"FirstName: {item}");
    Console.WriteLine("-----------------------------");
}

//Lấy ra danh sách các nhân viên có Salary lớn hơn 50000$.
var salaryRange = employees.Where(x => x.Salary > 50000);
foreach (var item in salaryRange)
{
    Console.WriteLine(item.Id);
    Console.WriteLine(item.FirstName);
    Console.WriteLine(item.LastNAme);
    Console.WriteLine(item.Salary);
    Console.WriteLine("--------------");
}

//Lấy ra danh sách các nhân viên có Gender là "Male" và sắp xếp tăng dần theo FirstName.
var male = employees.Where(x => x.Gender == "Male").OrderBy(x => x.FirstName);
foreach (var item in male)
{
    Console.WriteLine($"|ID: {item.Id}     |Name: {item.FirstName} {item.LastNAme}     |Gender: {item.Gender}");
}

//Lấy ra danh sách các nhân viên có Country là "Indonesia" và JobTitle chứa "Manager".
var country = employees.Where(x => x.country == "Indonesia" && x.JobTitle != null && x.JobTitle.Contains("Manager"));
foreach (var item in country)
{
    Console.WriteLine($"|ID: {item.Id}     |Name: {item.FirstName} {item.LastNAme}     |Country: {item.country}     |JobTitle: {item.JobTitle}");
}

//Lấy ra danh sách các nhân viên có Email và sắp xếp giảm dần theo LastName.
var email = employees.Where(x => x.Email != null).OrderByDescending(x => x.LastNAme);
foreach (var item in email)
{
    Console.WriteLine($"ID: {item.Id}");
    Console.WriteLine($"LastName: {item.LastNAme}");
    Console.WriteLine($"Email: {item.Email}");
    Console.WriteLine("-------------------------");
}

//Lấy ra danh sách các nhân viên có StartDate trước ngày "2022-01-01" và Salary lớn hơn 60000$.
var filteredEmployees = employees.Where(x => x.StartDate < new DateTime(2022, 1, 1) && x.Salary > 60000);
foreach (var employee in filteredEmployees)
{
    Console.WriteLine($"ID: {employee.Id}");
    Console.WriteLine($"Name: {employee.FirstName} {employee.LastNAme}");
    Console.WriteLine($"StartDate: {employee.StartDate}");
    Console.WriteLine($"Salary: {employee.Salary}");
    Console.WriteLine("------------------------------");
}

//Lấy ra danh sách các nhân viên có PostalCode là null hoặc rỗng và sắp xếp tăng dần theo LastName.
var filterPostalCode = employees.Where(x => string.IsNullOrEmpty(x.PostalCode)).OrderBy(x => x.LastNAme);
foreach (var item in filterPostalCode)
{
    Console.WriteLine($"ID: {item.Id}");
    Console.WriteLine($"Name: {item.FirstName} {item.LastNAme}");
    Console.WriteLine($"PostalCode: {item.PostalCode}");
    Console.WriteLine("------------------------------");
}

//Lấy ra danh sách các nhân viên có FirstName và LastName được viết hoa và sắp xếp giảm dần theo Id.
var filterName = employees.Where(x => x.FirstName.ToUpper() == x.FirstName && x.LastNAme.ToUpper() == x.LastNAme).OrderByDescending(x => x.Id);
foreach (var item in filterName)
{
    Console.WriteLine($"ID: {item}");
    Console.WriteLine($"Name: {item.FirstName} {item.LastNAme}");
    Console.WriteLine("------------------------------");
}

//Lấy ra danh sách các nhân viên có Salary nằm trong khoảng từ 50000$ đến 70000$ và sắp xếp tăng dần theo Salary.
var salaryRange = employees.Where(x => x.Salary > 50000 && x.Salary < 70000);
foreach (var item in salaryRange)
{
    Console.WriteLine(item.Id);
    Console.WriteLine(item.FirstName);
    Console.WriteLine(item.LastNAme);
    Console.WriteLine(item.Salary);
    Console.WriteLine("--------------");
}


//Lấy ra danh sách các nhân viên có FirstName chứa chữ "a" hoặc "A" và sắp xếp giảm dần theo LastName.
var containChar = employees.Where(x => x.FirstName.ToLower().Contains("a")).OrderByDescending(x => x.LastNAme);
foreach (var item in containChar)
{
    Console.WriteLine($"ID: {item.Id}");
    Console.WriteLine($"Name: {item.FirstName} {item.LastNAme}");
    Console.WriteLine("-------------------");
}
