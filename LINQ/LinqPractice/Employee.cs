﻿// đọc file mock-data.json trong project để lấy dữ liệu

public class Employee
{
    public int Id { get; set; }
    public string FirstName { get; set; }
    public string LastNAme { get; set; }
    public string? Email { get; set; }
    public string Gender { get; set; }
    public string country { get; set; }
    public string? PostalCode { get; set; }
    public string JobTitle { get; set; }
    public DateTime? StartDate { get; set; }
    public double Salary { get; set; }


}