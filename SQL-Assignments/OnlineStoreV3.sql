﻿--Create Database
CREATE DATABASE OnlineStoreDatabase;

USE OnlineStoreDatabase;

--Create Customers Table
CREATE TABLE Customers (
    ID INT PRIMARY KEY IDENTITY(1,1),
    FullName NVARCHAR(100),
    Email NVARCHAR(100),
    Address NVARCHAR(100)
);

--Create Products Table
CREATE TABLE Products (
    ID INT PRIMARY KEY IDENTITY(1,1),
    ProductName NVARCHAR(100),
    Price DECIMAL(10,2) CHECK (Price > 0),
    StockQuantity INT CHECK (StockQuantity >= 0)
);

--Create Orders Table
CREATE TABLE Orders (
    ID INT PRIMARY KEY IDENTITY(1,1),
    CustomerID INT,
    OrderDate DATETIME,
    TotalPrice DECIMAL(10,2),
    FOREIGN KEY (CustomerID) REFERENCES Customers(ID)
);

--Create OrderDetails Table
CREATE TABLE OrderDetails (
    ID INT PRIMARY KEY IDENTITY(1,1),
    OrderID INT,
    ProductID INT,
    Quantity INT,
    FOREIGN KEY (OrderID) REFERENCES Orders(ID),
    FOREIGN KEY (ProductID) REFERENCES Products(ID)
);


--Insert 10 records for each tables

--Insert into Customers
INSERT INTO Customers (FullName, Email, Address) VALUES ('Zebedee Iacovelli', 'ziacovelli0@abc.net.au', '827 Jenna Drive');
INSERT INTO Customers (FullName, Email, Address) VALUES ('Justinian Alford', 'jalford1@state.tx.us', '97 Division Court');
INSERT INTO Customers (FullName, Email, Address) VALUES ('Reider Davinet', 'rdavinet2@va.gov', '6 Bellgrove Parkway');
INSERT INTO Customers (FullName, Email, Address) VALUES ('Starlin Kenninghan', 'skenninghan3@a8.net', '00 Bowman Drive');
INSERT INTO Customers (FullName, Email, Address) VALUES ('Tod Markovich', 'tmarkovich4@apache.org', '7515 Ridgeway Terrace');
INSERT INTO Customers (FullName, Email, Address) VALUES ('Feodor Stairs', 'fstairs5@paypal.com', '31 Aberg Crossing');
INSERT INTO Customers (FullName, Email, Address) VALUES ('Ramona Ockendon', 'rockendon6@blog.com', '9078 Bartillon Circle');
INSERT INTO Customers (FullName, Email, Address) VALUES ('Albertina Akhurst', 'aakhurst7@bluehost.com', '1 Reindahl Alley');
INSERT INTO Customers (FullName, Email, Address) VALUES ('Katina Scougal', 'kscougal8@tripadvisor.com', '4881 Steensland Street');
INSERT INTO Customers (FullName, Email, Address) VALUES ('Ermanno Penning', 'epenning9@ca.gov', '4 Erie Junction');

--Insert into Products
INSERT INTO Products (ProductName, Price, StockQuantity) VALUES ('Seedlings - Buckwheat, Organic', '59.09', 5);
INSERT INTO Products (ProductName, Price, StockQuantity) VALUES ('Tobasco Sauce', '8.57', 8);
INSERT INTO Products (ProductName, Price, StockQuantity) VALUES ('Lid Coffeecup 12oz D9542b', '23.64', 6);
INSERT INTO Products (ProductName, Price, StockQuantity) VALUES ('Cumin - Ground', '6.47', 5);
INSERT INTO Products (ProductName, Price, StockQuantity) VALUES ('Oats Large Flake', '45.31', 8);
INSERT INTO Products (ProductName, Price, StockQuantity) VALUES ('Liners - Baking Cups', '90.28', 10);
INSERT INTO Products (ProductName, Price, StockQuantity) VALUES ('Persimmons', '77.76', 10);
INSERT INTO Products (ProductName, Price, StockQuantity) VALUES ('Sprouts - Bean', '88.76', 8);
INSERT INTO Products (ProductName, Price, StockQuantity) VALUES ('Pasta - Orzo, Dry', '51.12', 2);
INSERT INTO Products (ProductName, Price, StockQuantity) VALUES ('Ecolab - Hand Soap Form Antibac', '25.05', 10);

--Insert into Orders
INSERT INTO Orders (CustomerID, OrderDate, TotalPrice) VALUES (1, '2023-10-01', 150.00);
INSERT INTO Orders (CustomerID, OrderDate, TotalPrice) VALUES (2, '2023-10-02', 75.50);
INSERT INTO Orders (CustomerID, OrderDate, TotalPrice) VALUES (3, '2023-10-03', 120.75);
INSERT INTO Orders (CustomerID, OrderDate, TotalPrice) VALUES (4, '2023-10-04', 45.00);
INSERT INTO Orders (CustomerID, OrderDate, TotalPrice) VALUES (5, '2023-10-05', 200.25);
INSERT INTO Orders (CustomerID, OrderDate, TotalPrice) VALUES (6, '2023-10-06', 90.60);
INSERT INTO Orders (CustomerID, OrderDate, TotalPrice) VALUES (7, '2023-10-07', 180.00);
INSERT INTO Orders (CustomerID, OrderDate, TotalPrice) VALUES (8, '2023-10-08', 60.25);
INSERT INTO Orders (CustomerID, OrderDate, TotalPrice) VALUES (9, '2023-10-09', 135.40);
INSERT INTO Orders (CustomerID, OrderDate, TotalPrice) VALUES (10, '2023-10-10', 110.00);

--Insert into OrderDetails
INSERT INTO OrderDetails (OrderID, ProductID, Quantity) VALUES (1, 1, 2);
INSERT INTO OrderDetails (OrderID, ProductID, Quantity) VALUES (2, 2, 3);
INSERT INTO OrderDetails (OrderID, ProductID, Quantity) VALUES (3, 3, 4);
INSERT INTO OrderDetails (OrderID, ProductID, Quantity) VALUES (4, 4, 1);
INSERT INTO OrderDetails (OrderID, ProductID, Quantity) VALUES (5, 5, 5);
INSERT INTO OrderDetails (OrderID, ProductID, Quantity) VALUES (6, 6, 2);
INSERT INTO OrderDetails (OrderID, ProductID, Quantity) VALUES (7, 7, 3);
INSERT INTO OrderDetails (OrderID, ProductID, Quantity) VALUES (8, 8, 4);
INSERT INTO OrderDetails (OrderID, ProductID, Quantity) VALUES (9, 9, 1);
INSERT INTO OrderDetails (OrderID, ProductID, Quantity) VALUES (10, 10, 5);

--Test data
SELECT * FROM Customers
SELECT * FROM Products
SELECT * FROM Orders
SELECT * FROM OrderDetails


--3. Truy vấn SQL để lấy danh sách tên đầy đủ và email của tất cả khách hàng có địa chỉ chứa 
--thông tin là ở thành phố hoặc tỉnh bắt đầu bằng chữ "H" (ví dụ: Hà Nội, Hồ Chí Minh, Hà Tĩnh...) 
--và sắp xếp theo tên đầy đủ của khách hàng tăng dần.

SELECT c.FullName AS FullName, c.Email AS Email
FROM Customers c
WHERE c.FullName LIKE 'H%'
ORDER BY c.FullName

--4. Truy vấn SQL để lấy danh sách tên sản phẩm và giá của tất cả sản phẩm có giá lớn hơn 100 
--và số lượng trong kho ít hơn 50, sắp xếp theo giá sản phẩm giảm dần.

SELECT p.ProductName AS ProductName, p.Price AS Price
FROM Products p
WHERE p.Price > 100 AND p.StockQuantity < 50
ORDER BY p.Price DESC

--5. Truy vấn SQL để lấy danh sách tên đầy đủ và email của tất cả khách hàng chưa đặt đơn hàng nào, 
--sắp xếp theo tên đầy đủ của khách hàng (tăng dần).

SELECT c.FullName AS FullName, c.Email AS Email
FROM Customers c
LEFT JOIN Orders o ON o.CustomerID = c.ID
WHERE o.ID IS NULL
ORDER BY c.FullName ASC;

--6. Truy vấn SQL để lấy tên sản phẩm (ProductName), giá sản phẩm (Price), số lượng (Quantity) 
--Thông tin về các sản phẩm đã được mua trong các đơn hàng có giá trị tổng (TotalPrice) lớn hơn 500,000.

SELECT p.ProductName AS ProductName, p.Price AS Price, SUM(od.Quantity) AS Quantity
FROM Products p
JOIN OrderDetails od ON od.ProductID = p.ID
JOIN Orders o ON o.ID = od.OrderID
GROUP BY p.ProductName, p.Price
HAVING SUM(o.TotalPrice) > 500000;


--7.Lấy danh sách tên khách hàng và tháng. Mà trong tháng đó khách hàng này đó đã chi tiêu hơn 500,000 đồng cho tổng các đơn hàng của họ.

SELECT c.FullName AS CustomerName, MONTH(o.OrderDate) AS Month
FROM Customers c
JOIN Orders o ON c.ID = o.CustomerID
GROUP BY c.FullName, MONTH(o.OrderDate)
HAVING SUM(o.TotalPrice) > 500000;

