CREATE TABLE EMPLOYEE(
	EmpNo INT PRIMARY KEY,
	EmpName VARCHAR(255) NOT NULL,
	BirthDay DATETIME,
	Email VARCHAR(255) UNIQUE NOT NULL,
	DeptNo INT,
	MgrNo INT NOT NULL DEFAULT 0,
	StartDate DATETIME,
	Salary MONEY,
	Level INT CHECK(Level >=1 AND Level <=7),
	Status INT CHECK(Status >=0 AND Status <=2) DEFAULT 0,
	Note TEXT
);

 

 

CREATE TABLE EMP_SKILL(
    SkillNo INT,
    EmpNo INT,
    SkillLevel INT CHECK(SkillLevel >= 1 AND SkillLevel <= 3),
    RegDate DATETIME,
    Description TEXT,
    PRIMARY KEY (SkillNo, EmpNo),
    FOREIGN KEY (SkillNo) REFERENCES SKILL(SkillNo),
    FOREIGN KEY (EmpNo) REFERENCES EMPLOYEE(EmpNo)  -- Ensure 'Employee' is the correct table name
);

 

 

CREATE TABLE SKILL(
	SkillNo INT IDENTITY(1,1) PRIMARY KEY,
	SkillName VARCHAR(255),
	Note TEXT
);

 

CREATE TABLE DEPARTMENT(
	DeptNo INT IDENTITY(1,1) PRIMARY KEY,
	DeptName VARCHAR(255),
	Note TEXT
);
SELECT * FROM EMPLOYEE
SELECT * FROM DEPARTMENT

 

ALTER TABLE EMPLOYEE
ADD FOREIGN KEY (DeptNo) REFERENCES DEPARTMENT(DeptNo);

 

-- Add records to the EMPLOYEE table
INSERT INTO EMPLOYEE (EmpNo, EmpName, BirthDay, Email, DeptNo, MgrNo, StartDate, Salary, Level, Status, Note)
VALUES
    (1, 'John Doe', '1990-01-15', 'john.doe@example.com', 101, 1001, '2018-05-10', 60000, 5, 0, 'Employee note 1'),
    (2, 'Jane Smith', '1988-07-22', 'jane.smith@example.com', 102, 1002, '2019-03-20', 70000, 6, 0, 'Employee note 2'),
    (3, 'Mike Johnson', '1992-12-05', 'mike.johnson@example.com', 101, 1003, '2020-01-15', 55000, 4, 1, 'Employee note 3'),
    (4, 'Lisa Davis', '1995-04-10', 'lisa.davis@example.com', 103, 1004, '2017-09-02', 75000, 7, 0, 'Employee note 4'),
    (5, 'Tom Wilson', '1989-06-25', 'tom.wilson@example.com', 102, 1001, '2016-11-30', 65000, 6, 0, 'Employee note 5'),
    (6, 'Sarah Brown', '1993-09-08', 'sarah.brown@example.com', 101, 1002, '2021-02-15', 62000, 5, 0, 'Employee note 6'),
    (7, 'David Lee', '1987-03-20', 'david.lee@example.com', 103, 1003, '2018-04-05', 72000, 6, 0, 'Employee note 7'),
    (8, 'Emily White', '1990-11-12', 'emily.white@example.com', 102, 1004, '2019-06-30', 68000, 4, 0, 'Employee note 8');

 

-- Add records to the EMP_SKILL table
-- Insert records into the SKILL table
INSERT INTO SKILL (SkillNo, SkillName, Note)
VALUES
    (1, 'Programming', 'Programming skills'),
    (2, 'Database Management', 'Database skills'),
    (3, 'Web Development', 'Web development skills'),
    (4, 'Project Management', 'Project management skills'),
    (5, 'Data Analysis', 'Data analysis skills'),
    (6, 'Networking', 'Network administration skills'),
    (7, 'Graphic Design', 'Graphic design skills'),
    (8, 'Sales', 'Sales and marketing skills');

 

-- Update the EMP_SKILL INSERT statements with existing SkillNo values
-- Update the EMP_SKILL INSERT statements with existing SkillNo values
INSERT INTO EMP_SKILL (SkillNo, EmpNo, SkillLevel, RegDate, Description)
VALUES
    (1, 1, 3, '2019-05-01', 'Skilled programmer'),
    (2, 1, 2, '2019-05-01', 'Database management expert'),
    (3, 2, 1, '2020-06-01', 'Experienced web developer'),
    (4, 2, 3, '2020-06-01', 'Project management'),
    (5, 3, 2, '2021-01-15', 'Data analyst'),
    (6, 3, 1, '2021-01-15', 'Network administrator'),
    (7, 4, 3, '2017-09-02', 'Graphic designer'),
    (8, 4, 2, '2017-09-02', 'Sales'),
    (1, 5, 2, '2016-11-30', 'Programming'),
    (3, 6, 2, '2021-02-15', 'Web development'),
    (4, 6, 1, '2021-02-15', 'Project management'),
    (6, 7, 2, '2018-04-05', 'Network administration'),
    (8, 8, 2, '2019-06-30', 'Sales');

 

 

-- Add records to the DEPARTMENT table
INSERT INTO DEPARTMENT (DeptName, Note)
VALUES
    ('IT Department', 'Information Technology Department'),
    ('HR Department', 'Human Resources Department'),
    ('Finance Department', 'Finance and Accounting Department'),
    ('Marketing Department', 'Marketing and Sales Department'),
    ('Operations Department', 'Operations and Logistics Department'),
    ('Research and Development', 'R&D Department'),
    ('Customer Support', 'Customer support and service'),
    ('Quality Assurance', 'Quality control and assurance');

 

UPDATE EMPLOYEE
SET DeptNo = CASE
    WHEN DeptNo = 101 THEN 1
    WHEN DeptNo = 102 THEN 2
    WHEN DeptNo = 103 THEN 3
    WHEN DeptNo = 104 THEN 4
    WHEN DeptNo = 105 THEN 5
    WHEN DeptNo = 106 THEN 6
    WHEN DeptNo = 107 THEN 7
    WHEN DeptNo = 108 THEN 8
    ELSE DeptNo  -- If there are other values, leave them as is
END;

 

UPDATE EMPLOYEE
SET MgrNo = CASE
    WHEN MgrNo = 1001 THEN 1
    WHEN MgrNo = 1002 THEN 2
    WHEN MgrNo = 1003 THEN 3
    WHEN MgrNo = 1004 THEN 4
    ELSE MgrNo  -- If there are other values, leave them as is
END;

 

 

---Q2---
SELECT e.EmpName, e.Email, d.DeptName
FROM EMPLOYEE e
JOIN DEPARTMENT d ON d.DeptNo = e.DeptNo
WHERE DATEDIFF(MONTH, e.StartDate, GETDATE()) >= 6;

 

 

---Q3---
SELECT e.EmpName
FROM EMPLOYEE e
INNER JOIN SKILL s ON s.Note = e.Note
WHERE s.SkillName = 'C++' OR s.SkillName = '.NET';

 

 

---Q4---List all employee names, manager names, manager emails of those employees.
SELECT e.EmpName AS EmployeeName, 
       m.EmpName AS ManagerName, 
       m.Email AS ManagerEmail
FROM EMPLOYEE e
LEFT JOIN EMPLOYEE m ON m.EmpNo = e.MgrNo;

 

 

---Q5---Specify the departments which have >=2 employees, print out the list of departmentsí employees right after each department.
SELECT d.DeptName, e.EmpName
FROM DEPARTMENT d
LEFT JOIN EMPLOYEE e ON e.DeptNo = d.DeptNo
WHERE d.DeptNo IN (
    SELECT DeptNo
    FROM EMPLOYEE
    GROUP BY DeptNo
    HAVING COUNT(*) >= 2
)
ORDER BY d.DeptName, e.EmpName;

 

 

---Q6---
SELECT e.EmpName, e.Email, COUNT(s.SkillName) AS NumSkills
FROM EMPLOYEE e
LEFT JOIN EMP_SKILL es ON es.EmpNo = e.EmpNo
LEFT JOIN SKILL s ON es.SkillNo = s.SkillNo
GROUP BY e.EmpName, e.Email
ORDER BY e.EmpName;

 

 

---Q7---
SELECT e.EmpName, e.Email, e.BirthDay
FROM EMPLOYEE e
WHERE e.EmpNo IN (
    SELECT e.EmpNo
    FROM EMPLOYEE e
    INNER JOIN EMP_SKILL es ON e.EmpNo = es.EmpNo
    GROUP BY e.EmpNo
    HAVING COUNT(DISTINCT es.SkillNo) > 1
);

 

--Q8--
CREATE VIEW LIST_ALL_EMPLOYEE AS
SELECT E.EmpName, d.DeptName, s.SkillName
FROM EMPLOYEE e
INNER JOIN EMP_SKILL es ON es.EmpNo = e.EmpNo
INNER JOIN DEPARTMENT d ON d.DeptNo = e.DeptNo
INNER JOIN SKILL s ON s.Note = e.Note
WHERE e.Status = 1