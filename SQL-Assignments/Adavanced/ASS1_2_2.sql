-- Create the San_Pham table
CREATE TABLE Employee_Table  (
    Employee_Number INT PRIMARY KEY,
    Employee_Name VARCHAR(255) NOT NULL,
	Department_Number INT
);

 

-- Create the Khach_Hang table
CREATE TABLE Skill_Table (
    Skill_Code INT PRIMARY KEY,
    Skill_Name VARCHAR(255) NOT NULL,
);

 

-- Create the Don_Hang table with foreign keys
CREATE TABLE Employee_Skill_Table  (
    Employee_Number INT,
    Skill_Code INT,
	Registered DATETIME
);

 

CREATE TABLE Department   (
    Department_Number INT PRIMARY KEY,
    Department_Name VARCHAR(255),
);

 

-- Insert data into the Employee_Table
INSERT INTO Employee_Table (Employee_Number, Employee_Name, Department_Number)
VALUES
    (1, 'John Doe', 101),
    (2, 'Jane Smith', 102),
    (3, 'Mike Johnson', 101);

 

-- Insert data into the Skill_Table
INSERT INTO Skill_Table (Skill_Code, Skill_Name)
VALUES
    (1, 'Programming'),
    (2, 'Database Management'),
    (3, 'Web Development');

 

-- Insert data into the Employee_Skill_Table
INSERT INTO Employee_Skill_Table (Employee_Number, Skill_Code, Registered)
VALUES
    (1, 1, '2023-09-01'),
    (1, 2, '2023-09-02'),
    (2, 3, '2023-09-03');

 

-- Insert data into the Department
INSERT INTO Department (Department_Number, Department_Name)
VALUES
    (101, 'IT Department'),
    (102, 'HR Department'),
    (103, 'Finance Department');

 

 

SELECT * FROM Employee_Table

 

SELECT * FROM Employee_Table

 

SELECT * FROM Employee_Table

 

---Q2---
SELECT e.Employee_Name, s.Skill_Name
FROM Employee_Skill_Table es
INNER JOIN Employee_Table e ON es.Employee_Number = e.Employee_Number
INNER JOIN Skill_Table s ON s.Skill_Code = es.Skill_Code
WHERE s.Skill_Name = 'Java';

 

---Q3---
WITH DepartmentEmployeeCounts AS (
    SELECT
        d.Department_Name,
        e.Employee_Name,
        ROW_NUMBER() OVER (PARTITION BY d.Department_Name ORDER BY e.Employee_Name) AS EmployeeRank
    FROM Department d
    INNER JOIN Employee_Table e ON d.Department_Number = e.Department_Number
)

 

SELECT Department_Name, Employee_Name
FROM DepartmentEmployeeCounts
WHERE EmployeeRank <= 3
ORDER BY Department_Name, Employee_Name;

 

---Q4---
SELECT e.Employee_Number, e.Employee_Name
FROM Employee_Table e
WHERE e.Department_Number IN (
	SELECT e.Employee_Number
	FROM Employee_Table e
	INNER JOIN Employee_Skill_Table es ON es.Employee_Number = e.Employee_Number
	GROUP BY e.Employee_Number
	HAVING COUNT(DISTINCT es.Skill_Code) > 1
)

 

 

---Q5---
CREATE VIEW REQUIRE AS
SELECT e.Employee_Name, e.Employee_Number
FROM Employee_Table e
INNER JOIN Department d ON d.Department_Number = e.Department_Number
WHERE e.Employee_Number IN (
	SELECT e.Employee_Number
	FROM Employee_Table e
	INNER JOIN Employee_Skill_Table es ON es.Employee_Number = e.Employee_Number
	GROUP BY e.Employee_Number
	HAVING COUNT(DISTINCT es.Skill_Code) > 1
)