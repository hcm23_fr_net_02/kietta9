-- Create table Employee, Status = 1: are working
CREATE TABLE [dbo].[Employee](
	[EmpNo] [int] NOT NULL
,	[EmpName] [nchar](30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
,	[BirthDay] [datetime] NOT NULL
,	[DeptNo] [int] NOT NULL
, 	[MgrNo] [int]
,	[StartDate] [datetime] NOT NULL
,	[Salary] [money] NOT NULL
,	[Status] [int] NOT NULL
,	[Note] [nchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
,	[Level] [int] NOT NULL
) ON [PRIMARY]

 

GO

 

ALTER TABLE Employee 
ADD CONSTRAINT PK_Emp PRIMARY KEY (EmpNo)
GO

 

ALTER TABLE [dbo].[Employee]  
ADD  CONSTRAINT [chk_Level] 
	CHECK  (([Level]=(7) OR [Level]=(6) OR [Level]=(5) OR [Level]=(4) OR [Level]=(3) OR [Level]=(2) OR [Level]=(1)))
GO
ALTER TABLE [dbo].[Employee]  
ADD  CONSTRAINT [chk_Status] 
	CHECK  (([Status]=(2) OR [Status]=(1) OR [Status]=(0)))

 

GO
ALTER TABLE [dbo].[Employee]
ADD Email NCHAR(30) 
GO

 

ALTER TABLE [dbo].[Employee]
ADD CONSTRAINT chk_Email CHECK (Email IS NOT NULL)
GO

 

ALTER TABLE [dbo].[Employee] 
ADD CONSTRAINT chk_Email1 UNIQUE(Email)

 

GO
ALTER TABLE Employee
ADD CONSTRAINT DF_EmpNo DEFAULT 0 FOR EmpNo

 

GO
ALTER TABLE Employee
ADD CONSTRAINT DF_Status DEFAULT 0 FOR Status

 

GO
CREATE TABLE [dbo].[Skill](
	[SkillNo] [int] IDENTITY(1,1) NOT NULL
,	[SkillName] [nchar](30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
,	[Note] [nchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]

 

GO
ALTER TABLE Skill
ADD CONSTRAINT PK_Skill PRIMARY KEY (SkillNo)

 

GO
CREATE TABLE [dbo].[Department](
	[DeptNo] [int] IDENTITY(1,1) NOT NULL
,	[DeptName] [nchar](30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
,	[Note] [nchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]

 

GO
ALTER TABLE Department
ADD CONSTRAINT PK_Dept PRIMARY KEY (DeptNo)

 

GO
CREATE TABLE [dbo].[Emp_Skill](
	[SkillNo] [int] NOT NULL
,	[EmpNo] [int] NOT NULL
,	[SkillLevel] [int] NOT NULL
,	[RegDate] [datetime] NOT NULL
,	[Description] [nchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]

 

GO
ALTER TABLE Emp_Skill
ADD CONSTRAINT PK_Emp_Skill PRIMARY KEY (SkillNo, EmpNo)
GO

 

ALTER TABLE Employee  
ADD  CONSTRAINT [FK_1] FOREIGN KEY([DeptNo])
REFERENCES Department (DeptNo)

 

GO
ALTER TABLE Emp_Skill
ADD CONSTRAINT [FK_2] FOREIGN KEY ([EmpNo])
REFERENCES Employee([EmpNo])

 

GO
ALTER TABLE Emp_Skill
ADD CONSTRAINT [FK_3] FOREIGN KEY ([SkillNo])
REFERENCES Skill([SkillNo])

 

GO


 

--1--
INSERT INTO Employee (EmpNo, EmpName, BirthDay, DeptNo, MgrNo, StartDate, Salary, Status, Note, Level, Email)
VALUES
    (1, 'John Doe', '1990-01-15', 1, NULL, '2020-05-10', 60000, 1, 'Note 1', 5, 'john.doe@example.com'),
    (2, 'Jane Smith', '1988-04-25', 2, 1, '2019-03-20', 70000, 1, 'Note 2', 6, 'jane.smith@example.com'),
    (3, 'Mike Johnson', '1992-09-05', 1, NULL, '2021-01-08', 55000, 1, 'Note 3', 4, 'mike.johnson@example.com'),
    (4, 'Lisa Davis', '1985-11-12', 3, 2, '2018-08-15', 75000, 1, 'Note 4', 7, 'lisa.davis@example.com'),
    (5, 'Tom Wilson', '1987-07-20', 2, 1, '2017-06-02', 65000, 1, 'Note 5', 6, 'tom.wilson@example.com'),
    (6, 'Emily Brown', '1993-03-30', 3, 2, '2022-02-14', 72000, 1, 'Note 6', 5, 'emily.brown@example.com'),
    (7, 'David Lee', '1989-06-10', 1, NULL, '2021-12-05', 58000, 1, 'Note 7', 4, 'david.lee@example.com'),
    (8, 'Sarah Clark', '1991-12-18', 2, 1, '2020-10-22', 69000, 1, 'Note 8', 6, 'sarah.clark@example.com');

 

 

INSERT INTO Skill (SkillName, Note)
VALUES
    ('Programming', 'Programming skills'),
    ('Database Management', 'Database skills'),
    ('Web Development', 'Web development skills'),
    ('Project Management', 'Project management skills'),
    ('Data Analysis', 'Data analysis skills'),
    ('Network Administration', 'Network administration skills'),
    ('UI/UX Design', 'UI/UX design skills'),
    ('Digital Marketing', 'Digital marketing skills');

 

 

INSERT INTO Department (DeptName, Note)
VALUES
    ('IT Department', 'Information Technology Department'),
    ('HR Department', 'Human Resources Department'),
    ('Finance Department', 'Finance and Accounting Department'),
    ('Marketing Department', 'Marketing and Sales Department'),
    ('Operations Department', 'Operations and Logistics Department'),
    ('Research Department', 'Research and Development Department'),
    ('Customer Support Department', 'Customer Support Department'),
    ('Legal Department', 'Legal Affairs Department');

 

 

INSERT INTO Emp_Skill (SkillNo, EmpNo, SkillLevel, RegDate, Description)
VALUES
    (1, 1, 5, '2020-06-01', 'Skilled programmer'),
    (2, 1, 4, '2020-06-01', 'Database management expert'),
    (3, 2, 6, '2019-04-01', 'Experienced web developer'),
    (4, 3, 4, '2021-02-15', 'Project manager for multiple projects'),
    (5, 4, 7, '2018-09-01', 'Data analysis specialist'),
    (6, 5, 6, '2017-07-01', 'Network administration skills'),
    (7, 6, 5, '2022-03-15', 'UI/UX design expert'),
    (8, 7, 4, '2022-01-10', 'Digital marketing skills');

 

 

--2--
SELECT e.EmpName, e.Email, d.DeptName
FROM Employee e
INNER JOIN Department d ON d.DeptNo = e.DeptNo
WHERE DATEDIFF(MONTH, e.StartDate, GETDATE()) >= 6;

 

 

--3--
SELECT e.EmpName
FROM Employee e
INNER JOIN Skill s ON s.Note = e.Note
WHERE s.SkillName = 'C++' OR s.SkillName = '.NET';

 

 

--4--
SELECT e.EmpName AS EmployeeName, 
       m.EmpName AS ManagerName, 
       m.Email AS ManagerEmail
FROM Employee e
LEFT JOIN Employee m ON m.EmpNo = e.MgrNo;

 

--5--
SELECT d.DeptName, e.EmpName
FROM Department d
LEFT JOIN Employee e ON e.DeptNo = d.DeptNo
WHERE d.DeptNo IN (
    SELECT DeptNo
    FROM Employee
    GROUP BY DeptNo
    HAVING COUNT(*) >= 2
)
ORDER BY d.DeptName, e.EmpName;

 

--6--List all name, email and number of skills of the employees and sort ascending order by employee�s name.
SELECT e.EmpName, e.Email, COUNT(s.SkillName) AS NumSkills
FROM Employee e
LEFT JOIN Emp_Skill es ON es.EmpNo = e.EmpNo
LEFT JOIN Skill s ON es.SkillNo = s.SkillNo
GROUP BY e.EmpName, e.Email
ORDER BY e.EmpName;

 

 

--7--Use SUB-QUERY technique to list out the different employees (include name, email, birthday) who are working and have multiple skills.
SELECT e.EmpName, e.Email, e.BirthDay
FROM Employee e
WHERE e.EmpNo IN (
    SELECT e.EmpNo
    FROM Employee e
    INNER JOIN Emp_Skill es ON e.EmpNo = es.EmpNo
    GROUP BY e.EmpNo
    HAVING COUNT(DISTINCT es.SkillNo) > 1
);

 

--8--
CREATE VIEW LIST_ALL_EMPLOYEE AS
SELECT E.EmpName, d.DeptName, s.SkillName
FROM Employee e
INNER JOIN Emp_Skill es ON es.EmpNo = e.EmpNo
INNER JOIN Department d ON d.DeptNo = e.DeptNo
INNER JOIN Skill s ON s.Note = e.Note
WHERE e.Status = 1