CREATE DATABASE ABCManagement

CREATE TABLE Employee(
	EmployeeID INT IDENTITY(1,1) PRIMARY KEY,
	EmployeeFirstName VARCHAR(255) NOT NULL,
	EmployeeLastName VARCHAR(255) NOT NULL,
	EmployeeHiredDate DATETIME,
	EmployeeStatus BIT,
	SupervisorID INT,
	SocialSecurityNumber VARCHAR(255)
);


CREATE TABLE Project(
	ProjectID INT IDENTITY(1,1) PRIMARY KEY,
	ProjectName VARCHAR(255) NOT NULL,
	ProjectDescription VARCHAR(255) NOT NULL,
	ProjectDetail VARCHAR(255) NOT NULL,
	ProjectCompletedOn DATETIME
);

CREATE TABLE Project_Modules(
	ModuleID INT IDENTITY(1,1) PRIMARY KEY,
	ProjectID INT,
	EmployeeID INT,
	ProjectModulesDate DATETIME,
	ProjectModulesCompletedOn DATETIME,
	ProjectModulesDescription VARCHAR(255)
);

CREATE TABLE Work_Done(
	WorkDoneID INT IDENTITY(1,1) PRIMARY KEY,
	ModuleID INT,
	WorkDoneDate DATETIME,
	WorkDoneDescription VARCHAR(255) NOT NULL,
	WorkDoneStatus BIT
);

-- Adding foreign keys to the Employee table
ALTER TABLE Project_Modules
ADD CONSTRAINT FK_Project_Modules_Employee
FOREIGN KEY (EmployeeID)
REFERENCES Employee(EmployeeID);

ALTER TABLE Employee
ADD CONSTRAINT FK_Employee_Supervisor
FOREIGN KEY (SupervisorID)
REFERENCES Employee(EmployeeID);

ALTER TABLE Employee
DROP CONSTRAINT FK_Employee_Supervisor;

-- Adding foreign keys to the Project_Modules table
ALTER TABLE Project_Modules
ADD CONSTRAINT FK_Project_Modules_Project
FOREIGN KEY (ProjectID)
REFERENCES Project(ProjectID);

-- Adding foreign keys to the Work_Done table
ALTER TABLE Work_Done
ADD CONSTRAINT FK_Work_Done_Module
FOREIGN KEY (ModuleID)
REFERENCES Project_Modules(ModuleID);

