CREATE DATABASE CustomerDB;
GO

USE CustomerDB;

--CREATE TABLE

CREATE TABLE KhachHang (
    MaKH INT IDENTITY(1,1) PRIMARY KEY,
    HoTen NVARCHAR(255) NOT NULL,
    DiaChi NVARCHAR(255),
    SoDT NVARCHAR(20) NOT NULL,
    NgSinh DATETIME,
    DoanhSo MONEY,
    NgDk DATETIME
);

CREATE TABLE NhanVien (
    MaNV INT IDENTITY(1,1) PRIMARY KEY,
    HoTen NVARCHAR(255) NOT NULL,
    SoDT NVARCHAR(20) NOT NULL,
    NgVaoLam DATETIME
);

CREATE TABLE SanPham (
    MaSP INT IDENTITY(1,1) PRIMARY KEY,
    TenSP NVARCHAR(255) NOT NULL,
    DVT NVARCHAR(50) NOT NULL,
    NuocSX NVARCHAR(255),
    Gia MONEY
);

CREATE TABLE HoaDon (
    SoHD INT IDENTITY(1,1) PRIMARY KEY,
    NgHD DATETIME,
    MaKH INT,
    MaNV INT,
    TriGia MONEY
);

CREATE TABLE CTHD (
    SoHD INT,
    MaSP INT,
    SL INT
);

-- Add foreign key constraints

ALTER TABLE HoaDon
ADD CONSTRAINT FK_HoaDon_KhachHang
FOREIGN KEY (MaKH)
REFERENCES KhachHang(MaKH);

ALTER TABLE HoaDon
ADD CONSTRAINT FK_HoaDon_NhanVien
FOREIGN KEY (MaNV)
REFERENCES NhanVien(MaNV);

ALTER TABLE CTHD
ADD CONSTRAINT FK_CTHD_HoaDon
FOREIGN KEY (SoHD)
REFERENCES HoaDon(SoHD);

ALTER TABLE CTHD
ADD CONSTRAINT FK_CTHD_SanPham
FOREIGN KEY (MaSP)
REFERENCES SanPham(MaSP);


-- Add data
INSERT INTO KhachHang (HoTen, DiaChi, SoDT, NgSinh, DoanhSo, NgDk)
VALUES
    ('Customer 1', 'Address 1', '1234567890', '1990-01-01', 1000.50, '2023-09-29'),
    ('Customer 2', 'Address 2', '9876543210', '1985-05-15', 800.75, '2023-09-29'),
    ('Customer 3', 'Address 3', '5551112222', '2000-10-10', 1200.25, '2023-09-29');

INSERT INTO NhanVien (HoTen, SoDT, NgVaoLam)
VALUES
    ('Employee 1', '1112223333', '2020-03-15'),
    ('Employee 2', '4445556666', '2021-07-10'),
    ('Employee 3', '7778889999', '2019-01-20');

INSERT INTO SanPham (TenSP, DVT, NuocSX, Gia)
VALUES
    ('Product 1', 'Piece', 'Country 1', 10.50),
    ('Product 2', 'Box', 'Country 2', 25.75),
    ('Product 3', 'Pack', 'Country 3', 5.99);

INSERT INTO HoaDon (NgHD, MaKH, MaNV, TriGia)
VALUES
    ('2023-09-29', 1, 1, 50.00),
    ('2023-09-29', 2, 2, 75.25),
    ('2023-09-29', 3, 3, 30.50);

INSERT INTO CTHD (SoHD, MaSP, SL)
VALUES
    (1, 1, 2),
    (2, 2, 3),
    (3, 3, 4);


---Q1---
SELECT s.TenSP, s.NuocSX
FROM SanPham s
LEFT JOIN CTHD c ON c.MaSP = s.MaSP
LEFT JOIN HoaDon h ON h.SoHD = c.SoHD
WHERE s.NuocSX = 'Viet Nam' OR h.NgHD = '2023-05-23'

---Q2---
SELECT s.MaSP, s.TenSP
FROM SanPham s
LEFT JOIN CTHD c ON c.MaSP = s.MaSP
WHERE c.SL IS NULL OR c.SL = 0


---Q3---
SELECT h.SoHD
FROM HoaDon h
INNER JOIN CTHD c ON c.SoHD = h.SoHD
INNER JOIN SanPham s ON s.MaSP = c.MaSP
WHERE s.NuocSX = 'Viet Nam'
