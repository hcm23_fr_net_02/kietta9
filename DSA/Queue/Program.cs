﻿DemoQueue queue = new DemoQueue(5);

queue.EnQueue(1);
queue.EnQueue(2);
queue.EnQueue(3);
queue.EnQueue(4);
queue.EnQueue(5);
queue.EnQueue(6);

Console.WriteLine("Dequeue: " + queue.Dequeue());
Console.WriteLine("Dequeue: " + queue.Dequeue());
Console.WriteLine("Dequeue: " + queue.Dequeue());
Console.WriteLine("Dequeue: " + queue.Dequeue());
Console.WriteLine("Dequeue: " + queue.Dequeue());


Console.WriteLine("Peek: " + queue.Peek());
Console.WriteLine("Count: " + queue.Count);

class DemoQueue
{
    public int[] items;
    int top = -1;
    int bot = -1;
    int maxQueue;

    public DemoQueue(int maxQueue)
    {
        this.maxQueue = maxQueue;
        items = new int[maxQueue];
    }

    public bool IsFull() => bot - top + 1 >= maxQueue;

    public bool IsEmpty() => top == -1 && bot == -1;
    public void EnQueue(int item)
    {
        if (IsFull())
        {
            throw new Exception("Queue is full");
        }
        if (IsEmpty())
        {
            top = 0;
            bot = 0;
        }
        else
        {
            bot++;
        }
        items[bot] = item;
    }

    public int Dequeue()
    {
        if (IsEmpty())
        {
            throw new Exception("Queue is empty");
        }
        int valueAtTop = items[top];
        if (top == bot)
        {
            top = -1;
            bot = -1;
        }
        else
        {
            top++;
        }
        return valueAtTop;
    }

    public int Peek()
    {
        if (IsEmpty())
        {
            throw new Exception("Queue is empty");
        }
        return items[top];
    }

    public int Count => IsEmpty() ? 0 : (bot - top + 1);
}
