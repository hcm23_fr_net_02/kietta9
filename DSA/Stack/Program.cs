﻿Stack stack = new Stack(10);

stack.Push(1);
stack.Push(2);
stack.Push(3);
stack.Push(4);
stack.Push(5);

Console.WriteLine(stack.Pop());
Console.WriteLine(stack.Pop());
Console.WriteLine(stack.Pop());

Console.WriteLine(stack.Peek());
Console.WriteLine(stack.Count);

class Stack
{
    int[] stack;
    int top = -1;
    int maxStackSize;
    public Stack(int maxStackSize)
    {
        this.maxStackSize = maxStackSize;
        stack = new int[maxStackSize];
    }

    public bool IsEmpty() => top == -1;

    public bool IsFull() => top == maxStackSize - 1;

    public void Push(int item)
    {
        if (IsFull())
        {
            throw new Exception("Stack is full");
        }
        top++;
        stack[top] = item;
    }

    public int Pop()
    {
        if (IsEmpty())
        {
            throw new Exception("Stack is empty");
        }
        int topValue = stack[top];
        top--;
        return topValue;

    }

    public int Peek() //return top value but not remove
    {
        if (IsEmpty())
        {
            throw new Exception("Stack is empty");
        }
        return stack[top];
    }

    public int Count => top + 1;
}
