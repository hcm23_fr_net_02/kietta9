﻿namespace MovieManagement.Presentation.Presentation
{
    public interface IMoviePresentation
    {
        public void AddMovie();
        public void ViewMovies();
        public void SearchMovie();
    }
}
