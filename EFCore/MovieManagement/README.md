# Ứng Dụng Quản Lý Phim Và Đánh Giá Phim

## Mô Tả

Xây dựng một trang web đánh giá phim và cơ sở dữ liệu để lưu trữ dữ liệu về phim và đánh giá. Nhiệm vụ của tôi là viết một ứng dụng console để tương tác với cơ sở dữ liệu và thực hiện các chức năng sau.


1. **Xem Danh Sách Các Bộ Phim:**
    - Khi bạn chọn chức năng này, ứng dụng sẽ hiển thị danh sách các bộ phim có trong cơ sở dữ liệu. Danh sách sẽ được chia thành các trang riêng.
    - Sử dụng các phím sau:
        - "N" để xem trang tiếp theo.
        - "P" để quay lại trang trước.
        - "B" để quay lại menu chính.
    - Bạn có thể nhập ID của bộ phim để xem thông tin chi tiết, chỉnh sửa, xóa, thêm đánh giá, xem danh sách đánh giá hoặc thêm đánh giá cho bộ phim.
2. **Thêm Bộ Phim Mới:**
    - Bạn có thể thêm thông tin phim mới vào cơ sở dữ liệu bằng cách cung cấp các thông tin như:
        - Tựa đề (Title).
        - Tác giả (Author).
        - Năm xuất bản (Publication Year).
        - Quốc gia (Country).

3. **Lọc Sách Theo Tiêu Chí:**
    - Bạn có thể tùy chọn lọc danh sách sách theo nhiều tiêu chí như tên phim, đạo diễn, năm xuất bản và quốc gia.

## Hướng Dẫn Thực Thi Ứng Dụng

- Use SQLServer, Visual Studio 2022
- Set Infrastructure as Startup Project
- Tools --> NuGet Package Manager --> Package Manager Console
- Add-Migration nameMigration
- Update-Database
- Set Presentation as Startup Project
- Press F5


## Tác Giả

- Tên: Trương Anh Kiệt
- Liên hệ: truonganhkietdev@gmail.com

## Giấy Phép

Ứng dụng này được làm bởi tôi. Mua tôi ly cà phê để xem chi tiết.

---

