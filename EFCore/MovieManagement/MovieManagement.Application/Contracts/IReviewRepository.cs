﻿using MovieManagement.Domain.Entities;

namespace MovieManagement.Application.Contracts
{
    public interface IReviewRepository : IGenericRepository<Review>
    {
    }
}
