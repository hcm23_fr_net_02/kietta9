﻿using MovieManagement.Application.Services.Interfaces;
using MovieManagement.Domain.Entities;

namespace MovieManagement.Application.Services.Implements
{
    public class ReviewService : IReviewService
    {
        private readonly IUnitOfWork _unitOfWork;

        public ReviewService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public IEnumerable<Review> GetReviews(int movieId)
        {
            try
            {
                return _unitOfWork.Review.GetAll();
            }
            catch (Exception)
            {
                throw;
            }
        }
        public void AddReview(Review review)
        {
            try
            {
                Movie movieToAddReview = _unitOfWork.Movie.Get(review.MovieId);
                if (movieToAddReview is not null)
                {
                    review.Movie = movieToAddReview;
                    _unitOfWork.Review.Add(review);
                    _unitOfWork.Save();
                }
                else
                {
                    throw new Exception();
                }

            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
