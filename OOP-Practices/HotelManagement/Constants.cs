﻿public static class Constants
{
    //ADD CONSTANT
    public const string ADD_TYPE = "Enter Type: ";
    public const string ADD_PRICE = "Enter Price per Night: ";
    public const string ADD_STATUS = "Enter Status: ";
    public const string ADD_EXTENSION = "Enter Extensions (separated by comma): ";
    public const string ADD_SUCCESS = "Added successfully!";
    public const string ADD_FAIL = "Error occur when adding: ";

    //REMOVE CONSTANT
    public const string REMOVE_SUCCESS = "Removed successfully!";
    public const string REMOVE_FAIL = "Error occur when removing: ";

    //BOOKING
    public const string SELECT_ROOMS = "Please select at least one room.";

    //USER REQUIREMENT
    public const string ENTER_ROOMID = "Enter Room's Id: ";
    public const string ENTER_CUSTOMERID = "Enter Customer's Id: ";
    public const string ENTER_NUMBOFROOMS = "Enter number of rooms to book: ";
    public const string ENTER_IDOFROOM = "Enter Id of room ";
    public const string ENTER_NUMBSOFREQUIREMENTS = "Enter number of special requirements: ";
    public const string ENTER_SPECIALREQUIREMENT = "Enter special requirement ";
    public const string ENTER_NUMBOFNIGHTS = "Enter number of nights: ";
    public const string ENTER_BOOKINGID = "Enter Booking's Id: ";

    //INVALID INPUT
    public const string INVALID_INPUT = "Invalid input. Please enter again!";

    //EXCEPTION
    public const string EXCEPTION = "Error: ";
}