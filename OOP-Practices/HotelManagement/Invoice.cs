﻿class Invoice
{
    public int Id { get; set; }
    public double Total { get; set; }
    public List<string> UsedServices { get; set; }
}
