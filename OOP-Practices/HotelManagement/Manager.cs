﻿class Manager
{
    public List<Room> rooms = new List<Room>();
    List<Booking> bookings = new List<Booking>();
    public void AddNewRoom(Room newRoom)
    {
        try
        {
            Console.Write(Constants.ADD_TYPE);
            newRoom.Type = Console.ReadLine();

            Console.Write(Constants.ADD_PRICE);
            if (double.TryParse(Console.ReadLine(), out double pricePerNight))
            {
                newRoom.PricePerNight = pricePerNight;
            }
            else
            {
                Console.WriteLine(Constants.INVALID_INPUT);
                return;
            }

            Console.Write(Constants.ADD_STATUS);
            newRoom.Status = Console.ReadLine();

            Console.Write(Constants.ADD_EXTENSION);
            string extensionsInput = Console.ReadLine();
            newRoom.Extensions = extensionsInput.Split(',').ToList();

            newRoom.Id = rooms.Count + 1;

            rooms.Add(newRoom);

            Console.WriteLine(Constants.ADD_SUCCESS);
        }
        catch (Exception ex)
        {
            Console.WriteLine(Constants.ADD_FAIL + ex.Message);
        }
    }

    public void RemoveRoom(int roomToRemove)
    {
        try
        {
            var removeRoom = rooms.FirstOrDefault(x => x.Id == roomToRemove);
            if (removeRoom is not null)
            {
                rooms.Remove(removeRoom);
                Console.WriteLine(Constants.REMOVE_SUCCESS);
            }
            else
            {
                Console.WriteLine(Constants.INVALID_INPUT);
            }
        }
        catch (Exception ex)
        {
            Console.WriteLine(Constants.REMOVE_FAIL + ex.Message);
        }
    }

    public void DisplayRooms()
    {
        foreach (var room in rooms)
        {
            Console.WriteLine($"Room - {room.Id}, Type - {room.Type}, Price Per Night - {room.PricePerNight:C}, Status - {room.Status}");
            Console.WriteLine("Facilities: " + string.Join(", ", room.Extensions));
        }
    }

    public Booking MakeBooking(int customerId, List<Room> selectedRooms, List<string> specialRequirements, int numberOfNights)
    {
        if (selectedRooms.Count == 0)
        {
            Console.WriteLine(Constants.SELECT_ROOMS);
            return null;
        }

        double totalCost = CalculateTotalCost(selectedRooms, numberOfNights);

        var booking = new Booking
        {
            Id = GenerateBookingId(),
            CustomerId = customerId,
            BookedAt = DateTime.Now,
            CheckedInAt = DateTime.MinValue,
            Booked = selectedRooms,
            SpecialRequire = specialRequirements,
            Invoice = new Invoice
            {
                Id = GenerateInvoiceId(),
                Total = totalCost,
                UsedServices = specialRequirements
            }
        };

        foreach (var room in selectedRooms)
        {
            var bookedRoom = rooms.FirstOrDefault(r => r.Id == room.Id);
            if (bookedRoom != null)
            {
                bookedRoom.Status = "Booked";
            }
        }

        bookings.Add(booking);

        return booking;
    }

    private double CalculateTotalCost(List<Room> selectedRooms, int numberOfNights)
    {
        double totalCost = 0;

        foreach (var room in selectedRooms)
        {
            totalCost += room.PricePerNight * numberOfNights;
        }

        return totalCost;
    }

    private int GenerateBookingId()
    {
        var randomId = Random.Shared.Next(1000, 9999);
        return randomId;
    }

    private int GenerateInvoiceId()
    {
        var randomId = Random.Shared.Next(1000, 9999);
        return randomId;
    }

    public void DisplayBookings()
    {
        foreach (var booking in bookings)
        {
            Console.WriteLine($"Booking Id: {booking.Id}, Customer Id: {booking.CustomerId}, Booked At: {booking.BookedAt}, Checked In At: {booking.CheckedInAt}");
            Console.WriteLine("Booked Rooms: " + string.Join(", ", booking.Booked.Select(r => r.Id)));
            Console.WriteLine("Special Requirements: " + string.Join(", ", booking.SpecialRequire));
            Console.WriteLine();
        }
    }

    public void GenerateAndDisplayInvoice(int bookingId)
    {
        var booking = bookings.FirstOrDefault(b => b.Id == bookingId);
        if (booking != null)
        {
            Console.WriteLine($"Invoice Id: {booking.Invoice.Id}, Total: {booking.Invoice.Total:C}");
            Console.WriteLine("Used Services: " + string.Join(", ", booking.Invoice.UsedServices));
            Console.WriteLine();
        }
        else
        {
            Console.WriteLine(Constants.INVALID_INPUT);
        }
    }
}
