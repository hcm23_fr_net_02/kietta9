﻿class Room
{
    public int Id { get; set; }
    public string Type { get; set; }
    public double PricePerNight { get; set; }
    public string Status { get; set; }
    public List<string> Extensions { get; set; }
}
