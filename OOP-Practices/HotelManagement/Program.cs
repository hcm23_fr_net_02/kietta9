﻿Console.OutputEncoding = System.Text.Encoding.UTF8;

Manager manager = new Manager();
List<Room> rooms = new List<Room>();
int choice;

try
{
    do
    {
        Menu.Display();

        choice = Convert.ToInt32(Console.ReadLine());

        switch (choice)
        {
            case 1:
                Room room = new Room();
                manager.AddNewRoom(room);
                break;

            case 2:
                Console.Write(Constants.ENTER_ROOMID);
                int id = Convert.ToInt32(Console.ReadLine());
                manager.RemoveRoom(id);
                break;

            case 3:
                manager.DisplayRooms();
                break;

            case 4:
                Console.Write(Constants.ENTER_CUSTOMERID);
                int customerId = Convert.ToInt32(Console.ReadLine());
                Console.Write(Constants.ENTER_NUMBOFROOMS);
                int numberOfRooms = Convert.ToInt32(Console.ReadLine());
                var selectedRooms = new List<Room>();
                for (int i = 0; i < numberOfRooms; i++)
                {
                    Console.Write(Constants.ENTER_IDOFROOM + (i + 1));
                    int roomId = Convert.ToInt32(Console.ReadLine());
                    var roomCheck = manager.rooms.FirstOrDefault(x => x.Id == roomId);
                    if (roomCheck != null)
                    {
                        selectedRooms.Add(roomCheck);
                    }
                    else
                    {
                        Console.WriteLine(Constants.INVALID_INPUT);
                    }
                }
                Console.Write(Constants.ENTER_NUMBSOFREQUIREMENTS);
                int numberOfRequirements = Convert.ToInt32(Console.ReadLine());
                var specialRequirements = new List<string>();
                for (int i = 0; i < numberOfRequirements; i++)
                {
                    Console.Write(Constants.ENTER_SPECIALREQUIREMENT + (i + 1));
                    string requirement = Console.ReadLine();
                    specialRequirements.Add(requirement);
                }
                Console.Write(Constants.ENTER_NUMBOFNIGHTS);
                int numberOfNights = Convert.ToInt32(Console.ReadLine());
                manager.MakeBooking(customerId, selectedRooms, specialRequirements, numberOfNights);
                break;

            case 5:
                manager.DisplayBookings();
                break;

            case 6:
                Console.Write(Constants.ENTER_BOOKINGID);
                int bookingId = Convert.ToInt32(Console.ReadLine());
                manager.GenerateAndDisplayInvoice(bookingId);
                break;

            case 7:
                return;

            default:
                break;
        }
    } while (choice != 7);
}
catch (Exception ex)
{

    Console.WriteLine(Constants.EXCEPTION + ex.Message); ;
}

