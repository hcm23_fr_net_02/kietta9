﻿class Menu
{
    public static void Display()
    {
        Console.WriteLine("***** HOTEL *****");
        Console.WriteLine("1. Add new room");
        Console.WriteLine("2. Delete room");
        Console.WriteLine("3. View list rooms");
        Console.WriteLine("4. Booking ");
        Console.WriteLine("5. View booked rooms");
        Console.WriteLine("6. Create invoice");
        Console.WriteLine("7. Exit");
        Console.Write("Your choice: ");
    }
}
