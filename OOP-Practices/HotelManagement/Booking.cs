﻿class Booking
{
    public int Id { get; set; }
    public int CustomerId { get; set; }
    public DateTime BookedAt { get; set; }
    public DateTime CheckedInAt { get; set; }
    public List<Room> Booked { get; set; }
    public List<string> SpecialRequire { get; set; }
    public Invoice Invoice { get; set; }
}
