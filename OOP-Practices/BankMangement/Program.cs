﻿class Customer
{
    private string Name;
    private string Email;
    private double AccountBalance;

    public double GetAccountBalacne() => AccountBalance;

    private void SetAccountBalance(double accountBalance)
    {
        if (accountBalance > 0)
        {
            AccountBalance = accountBalance;
        }
        else
        {
            throw new Exception("AccountBalance must be greater than 0");
        }
    }

    public Customer(string name, string email, double accountBalance)
    {
        Name = name;
        Email = email;
        SetAccountBalance(accountBalance);
    }

    public void AddFunds(double amount)
    {
        if (amount > 0)
        {
            AccountBalance += amount;
        }
        else
        {
            throw new ArgumentException("Amount must be greater than 0");
        }
    }

    public void MakePurchase(double amount)
    {
        AccountBalance -= amount;
    }

    public bool CanAfford(double amount)
    {
        return AccountBalance >= amount;
    }

}
