﻿using Newtonsoft.Json;

class Book
{
    [JsonProperty("Id")]
    private int Id;
    [JsonProperty("Title")]
    private string Title;
    [JsonProperty("Author")]
    private string Author;
    [JsonProperty("Available")]
    private bool Available;

    public int GetId()
    {
        return Id;
    }

    public void SetId(int id)
    {
        Id = id;
    }

    public string GetTitle()
    {
        return Title;
    }

    public void SetTitle(string title)
    {
        Title = title;
    }

    public string GetAuthor()
    {
        return Author;
    }

    public void SetAuthor(string author)
    {
        Author = author;
    }

    public bool IsAvailable()
    {
        return Available;
    }

    public void SetAvailable(bool available)
    {
        Available = available;
    }

    public Book()
    {

    }
}
