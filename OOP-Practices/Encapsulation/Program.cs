﻿Console.OutputEncoding = System.Text.Encoding.UTF8;

////Get filepath
//string jsonFilePath = "MOCK_DATA.json";

////Read this file content
//string jsonContent = File.ReadAllText(jsonFilePath);

//// Add file content into Object list
//List<Book>? books = JsonConvert.DeserializeObject<List<Book>>(jsonContent);

Library library = new Library();

Book book1 = new Book();
book1.SetId(1);
book1.SetTitle("Book 1");
book1.SetAuthor("Author 1");
book1.SetAvailable(true);

Book book2 = new Book();
book2.SetId(2);
book2.SetTitle("Book 2");
book2.SetAuthor("Author 2");
book2.SetAvailable(true);

library.books.Add(book1);
library.books.Add(book2);

while (true)
{
    Menu.Display();

    int choice = Convert.ToInt32(Console.ReadLine());

    switch (choice)
    {
        case 1:
            Console.Write("Nhập ID sách muốn mượn: ");
            int checkOutId = Convert.ToInt32(Console.ReadLine());
            library.CheckOut(checkOutId);
            break;

        case 2:
            Console.Write("Nhập ID sách muốn trả: ");
            int returnId = Convert.ToInt32(Console.ReadLine());
            library.ReturnBook(returnId);
            break;

        case 3:
            library.PrintBookInfo();
            break;

        case 4:
            return;

        default:
            Console.WriteLine("Lựa chọn không hợp lệ.");
            break;
    }
}
