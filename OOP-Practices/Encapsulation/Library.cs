﻿class Library
{
    private List<Book> books = new List<Book>();

    public void CheckOut(int id)
    {
        Book book = books.Find(b => b.GetId() == id && b.IsAvailable());
        if (book != null)
        {
            book.SetAvailable(false);
            Console.WriteLine("Mượn sách thành công.");
        }
        else
        {
            Console.WriteLine("Không thể mượn sách.");
        }
    }

    public void ReturnBook(int id)
    {
        Book book = books.Find(b => b.GetId() == id);
        if (book != null)
        {
            book.SetAvailable(true);
            Console.WriteLine("Trả sách thành công.");
        }
        else
        {
            Console.WriteLine("Không thể trả sách.");
        }
    }

    public void PrintBookInfo()
    {
        Console.WriteLine("Danh sách sách trong thư viện:");
        foreach (Book book in books)
        {
            Console.WriteLine($"ID: {book.GetId()}, Title: {book.GetTitle()}, Author: {book.GetAuthor()}, Available: {book.IsAvailable()}");
        }
    }
}