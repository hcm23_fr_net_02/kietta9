﻿class Customer
{
    public string Name { get; set; }
    public string Address { get; set; }
    public int MemberCode { get; set; }

    public Customer(string name, string address, int memberCode)
    {
        Name = name;
        Address = address;
        MemberCode = memberCode;
    }
}
