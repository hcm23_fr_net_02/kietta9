﻿public class Constants
{
    public const string AddBookPrompt = "Tên sách: ";
    public const string AuthorPrompt = "Tác giả: ";
    public const string StockQuantityPrompt = "Số lượng tồn kho: ";
    public const string PricePrompt = "Giá tiền: ";
    public const string BookCodePrompt = "Mã số sách: ";
    public const string DeleteBookPrompt = "Nhập mã số sách cần xóa: ";
    public const string CustomerNamePrompt = "Tên khách hàng: ";
    public const string CustomerAddressPrompt = "Địa chỉ: ";
    public const string MemberCodePrompt = "Mã số thành viên: ";
    public const string BorrowBookPrompt = "Nhập mã số sách để mượn (hoặc 0 để kết thúc): ";
    public const string LoanSuccessMessage = "Mượn sách thành công!";
    public const string LoanDueDate = "Ngày hết hạn: ";
    public const string FindBookByCodePrompt = "Nhập mã số sách để tìm kiếm: ";
    public const string NotFoundMessage = "Không tìm thấy sách với mã số này.";
    public const string AddBookMessage = "Thêm sách thành công!";
    public const string RemoveBookMessage = "Xóa sách thành công!";
}
