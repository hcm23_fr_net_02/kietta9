﻿class Menu
{
    public static void Display()
    {
        Console.WriteLine("Chọn chức năng:");
        Console.WriteLine("1. Thêm sách mới vào thư viện.");
        Console.WriteLine("2. Xóa sách khỏi thư viện.");
        Console.WriteLine("3. Hiển thị danh sách các cuốn sách có trong thư viện.");
        Console.WriteLine("4. Cho phép một khách hàng mượn sách.");
        Console.WriteLine("5. Hiển thị thông tin về các mượn sách hiện có.");
        Console.WriteLine("6. Tìm kiếm sách theo mã số.");
        Console.WriteLine("7. Thoát chương trình.");
        Console.Write("Bạn chọn: ");
    }
}
