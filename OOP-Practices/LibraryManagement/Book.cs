﻿class Book
{
    public string Title { get; set; }
    public string Author { get; set; }
    public int StockQuantity { get; set; }
    public decimal Price { get; set; }
    public int BookCode { get; set; }

    public Book(string title, string author, int stockQuantity, decimal price, int bookCode)
    {
        Title = title;
        Author = author;
        StockQuantity = stockQuantity;
        Price = price;
        BookCode = bookCode;
    }
}
