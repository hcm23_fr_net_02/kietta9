﻿class Library
{
    public List<Book> Books { get; set; }

    public Library()
    {
        Books = new List<Book>();
    }

    public void AddBook(Book book)
    {
        Books.Add(book);
    }

    public void RemoveBook(int bookCode)
    {
        Book bookToRemove = Books.Find(book => book.BookCode == bookCode);
        if (bookToRemove != null)
        {
            Books.Remove(bookToRemove);
        }
    }

    public Book FindBookByBookCode(int bookCode)
    {
        return Books.Find(book => book.BookCode == bookCode);
    }
}
