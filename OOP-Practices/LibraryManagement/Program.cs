﻿try
{
    Library library = new Library();
    List<Loan> loans = new List<Loan>();

    while (true)
    {
        Console.OutputEncoding = System.Text.Encoding.UTF8;

        Menu.Display();
        int choice = Convert.ToInt32(Console.ReadLine());

        switch (choice)
        {
            case 1:
                // Thêm sách mới vào thư viện
                Console.Write(Constants.AddBookPrompt);
                string bookTitle = Console.ReadLine();
                Console.Write(Constants.AuthorPrompt);
                string author = Console.ReadLine();
                Console.Write(Constants.StockQuantityPrompt);
                int stockQuantity = Convert.ToInt32(Console.ReadLine());
                Console.Write(Constants.PricePrompt);
                decimal price = Convert.ToDecimal(Console.ReadLine());
                Console.Write(Constants.BookCodePrompt);
                int bookCode = Convert.ToInt32(Console.ReadLine());

                Book newBook = new Book(bookTitle, author, stockQuantity, price, bookCode);
                library.AddBook(newBook);
                Console.WriteLine(Constants.AddBookMessage);
                break;

            case 2:
                // Xóa sách khỏi thư viện
                Console.Write(Constants.DeleteBookPrompt);

                int bookCodeToDelete = Convert.ToInt32(Console.ReadLine());
                library.RemoveBook(bookCodeToDelete);
                Console.WriteLine(Constants.RemoveBookMessage);
                break;

            case 3:
                // Hiển thị danh sách sách trong thư viện
                Console.WriteLine("Danh sách các cuốn sách trong thư viện:");
                if (library.Books.Count == 0)
                {
                    Console.WriteLine("Chưa có quyển sách nào");
                }
                else
                {
                    foreach (Book book in library.Books)
                    {
                        Console.WriteLine($"Tên sách: {book.Title}, Tác giả: {book.Author}, Giá: {book.Price}, Số lượng tồn kho: {book.StockQuantity}, Mã số sách: {book.BookCode}");
                    }
                }
                break;

            case 4:
                // Cho phép một khách hàng mượn sách
                Console.Write(Constants.CustomerNamePrompt);
                string customerName = Console.ReadLine();
                Console.Write(Constants.CustomerAddressPrompt);
                string customerAddress = Console.ReadLine();
                Console.Write(Constants.MemberCodePrompt);
                int memberCode = Convert.ToInt32(Console.ReadLine());

                Customer customer = new Customer(customerName, customerAddress, memberCode);

                List<Book> borrowedBooks = new List<Book>();
                while (true)
                {
                    Console.Write(Constants.BorrowBookPrompt);
                    int bookCodeToBorrow = Convert.ToInt32(Console.ReadLine());
                    if (bookCodeToBorrow == 0)
                        break;

                    Book bookToBorrow = library.FindBookByBookCode(bookCodeToBorrow);
                    if (bookToBorrow != null)
                    {
                        borrowedBooks.Add(bookToBorrow);
                        bookToBorrow.StockQuantity--;
                    }
                    else
                    {
                        Console.WriteLine(Constants.NotFoundMessage);
                    }
                }

                int loanCode = loans.Count + 1;
                DateTime loanDate = DateTime.Now;
                DateTime dueDate = loanDate.AddDays(14); // Số ngày mượn là 2 tuần
                Loan loan = new Loan(loanCode, loanDate, dueDate, borrowedBooks);
                loans.Add(loan);

                Console.WriteLine(Constants.LoanSuccessMessage);
                break;

            case 5:
                // Hiển thị thông tin về các mượn sách hiện có
                Console.WriteLine("Thông tin về các mượn sách hiện có:");
                foreach (Loan loanInfo in loans)
                {
                    Console.WriteLine($"Mã số mượn: {loanInfo.LoanCode}");
                    Console.WriteLine($"Ngày mượn: {loanInfo.LoanDate}");
                    Console.WriteLine($"Ngày hết hạn: {loanInfo.DueDate}");
                    Console.WriteLine("Danh sách các cuốn sách đã mượn:");
                    foreach (Book book in loanInfo.BorrowedBooks)
                    {
                        Console.WriteLine($"Tên sách: {book.Title}, Giá: {book.Price}");
                    }
                    Console.WriteLine();
                }
                break;

            case 6:
                // Tìm kiếm sách theo mã số
                Console.Write(Constants.FindBookByCodePrompt);
                int bookCodeToSearch = Convert.ToInt32(Console.ReadLine());
                Book foundBook = library.FindBookByBookCode(bookCodeToSearch);
                if (foundBook != null)
                {
                    Console.WriteLine($"Tên sách: {foundBook.Title}, Tác giả: {foundBook.Author}, Giá: {foundBook.Price}, Số lượng tồn kho: {foundBook.StockQuantity}, Mã số sách: {foundBook.BookCode}");
                }
                else
                {
                    Console.WriteLine(Constants.NotFoundMessage);
                }
                break;

            case 7:
                // Thoát chương trình
                Environment.Exit(7);
                break;

            default:
                Console.WriteLine("Lựa chọn không hợp lệ. Vui lòng chọn lại.");
                break;
        }
    }
}
catch (Exception ex)
{
    Console.WriteLine($"Lỗi: {ex.Message}"); ;
}
