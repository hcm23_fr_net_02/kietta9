﻿class Loan
{
    public int LoanCode { get; set; }
    public DateTime LoanDate { get; set; }
    public DateTime DueDate { get; set; }
    public List<Book> BorrowedBooks { get; set; }

    public Loan(int loanCode, DateTime loanDate, DateTime dueDate, List<Book> borrowedBooks)
    {
        LoanCode = loanCode;
        LoanDate = loanDate;
        DueDate = dueDate;
        BorrowedBooks = borrowedBooks;
    }
}
