﻿class Purchase
{
    public int PurchaseCode { get; set; }
    public DateTime PurchaseDate { get; set; }
    public List<MobilePhone> PurchasedPhones { get; set; }
}
  