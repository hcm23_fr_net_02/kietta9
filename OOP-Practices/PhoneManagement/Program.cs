﻿Console.OutputEncoding = System.Text.Encoding.UTF8;

Store store = new Store();
List<Customer> customers = new List<Customer>();
List<Purchase> purchases = new List<Purchase>();

int purchaseCode = 1;

while (true)
{
    Menu.Display();

    int choice = Convert.ToInt32(Console.ReadLine());

    switch (choice)
    {
        case 1:
            MobilePhone phone = new MobilePhone();
            Console.Write("Nhập tên điện thoại: ");
            phone.Name = Console.ReadLine();
            Console.Write("Nhập hãng sản xuất: ");
            phone.Manufacturer = Console.ReadLine();
            Console.Write("Nhập số lượng tồn kho: ");
            phone.StockQuantity = Convert.ToInt32(Console.ReadLine());
            Console.Write("Nhập giá tiền: ");
            phone.Price = Convert.ToDecimal(Console.ReadLine());
            Console.Write("Nhập mã số sản phẩm: ");
            phone.ProductCode = Convert.ToInt32(Console.ReadLine());
            store.AddMobilePhone(phone);
            break;

        case 2:
            Console.Write("Nhập mã số sản phẩm của điện thoại cần xóa: ");
            int productCode = Convert.ToInt32(Console.ReadLine());
            store.RemoveMobilePhone(productCode);
            break;

        case 3:
            store.DisplayMobilePhones();
            break;

        case 4:
            Customer customer = new Customer();
            Console.Write("Nhập tên khách hàng: ");
            customer.Name = Console.ReadLine();
            Console.Write("Nhập địa chỉ: ");
            customer.Address = Console.ReadLine();
            Console.Write("Nhập mã số thành viên: ");
            customer.MemberCode = Convert.ToInt32(Console.ReadLine());

            Purchase purchase = new Purchase();
            purchase.PurchaseCode = purchaseCode++;
            purchase.PurchaseDate = DateTime.Now;
            purchase.PurchasedPhones = new List<MobilePhone>();

            while (true)
            {
                store.DisplayMobilePhones();
                Console.Write("Nhập mã số sản phẩm điện thoại bạn muốn mua (hoặc 0 để hoàn thành): ");
                int productCodeToPurchase = Convert.ToInt32(Console.ReadLine());
                if (productCodeToPurchase == 0)
                {
                    break;
                }
                MobilePhone phoneToPurchase = store.GetMobilePhoneByProductCode(productCodeToPurchase);
                if (phoneToPurchase != null)
                {
                    purchase.PurchasedPhones.Add(phoneToPurchase);
                    phoneToPurchase.StockQuantity--;
                }
                else
                {
                    Console.WriteLine("Không tìm thấy điện thoại với mã số sản phẩm đã cho.");
                }
            }

            customers.Add(customer);
            purchases.Add(purchase);
            break;

        case 5:
            Console.WriteLine("Thông tin mua hàng:");
            foreach (Purchase p in purchases)
            {
                Console.WriteLine($"Mã số mua hàng: {p.PurchaseCode}, Ngày mua hàng: {p.PurchaseDate}");
                Console.WriteLine("Các điện thoại đã mua:");
                foreach (MobilePhone item in p.PurchasedPhones)
                {
                    Console.WriteLine($"Mã số sản phẩm: {item.ProductCode}, Tên: {item.Name}, Hãng sản xuất: {item.Manufacturer}, Giá tiền: {item.Price}");
                }
            }
            break;

        case 6:
            Console.Write("Nhập mã số sản phẩm điện thoại cần tìm: ");
            int searchProductCode = Convert.ToInt32(Console.ReadLine());
            MobilePhone foundPhone = store.GetMobilePhoneByProductCode(searchProductCode);
            if (foundPhone != null)
            {
                Console.WriteLine($"Thông tin điện thoại tìm thấy: Mã số sản phẩm: {foundPhone.ProductCode}, Tên: {foundPhone.Name}, Hãng sản xuất: {foundPhone.Manufacturer}, Số lượng tồn kho: {foundPhone.StockQuantity}, Giá tiền: {foundPhone.Price}");
            }
            else
            {
                Console.WriteLine("Không tìm thấy điện thoại với mã số sản phẩm đã cho.");
            }
            break;

        case 7:
            return;

        default:
            Console.WriteLine("Lựa chọn không hợp lệ.");
            break;
    }
}
