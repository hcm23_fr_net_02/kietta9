﻿public class Store
{
    private List<MobilePhone> phones = new List<MobilePhone>();

    public void AddMobilePhone(MobilePhone phone)
    {
        phones.Add(phone);
    }

    public void RemoveMobilePhone(int productCode)
    {
        MobilePhone phoneToRemove = phones.FirstOrDefault(phone => phone.ProductCode == productCode);
        if (phoneToRemove != null)
        {
            phones.Remove(phoneToRemove);
        }
        else
        {
            Console.WriteLine("Không tìm thấy điện thoại với mã số sản phẩm đã cho.");
        }
    }

    public void DisplayMobilePhones()
    {
        Console.WriteLine("Danh sách các điện thoại trong cửa hàng:");
        foreach (MobilePhone phone in phones)
        {
            Console.WriteLine($"Mã số sản phẩm: {phone.ProductCode}, Tên: {phone.Name}, Hãng sản xuất: {phone.Manufacturer}, Số lượng tồn kho: {phone.StockQuantity}, Giá tiền: {phone.Price}");
        }
    }

    public MobilePhone GetMobilePhoneByProductCode(int productCode)
    {
        return phones.Find(phone => phone.ProductCode == productCode);
    }
}
