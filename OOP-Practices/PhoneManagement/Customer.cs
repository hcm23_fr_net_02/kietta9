﻿class Customer
{
    public string Name { get; set; }
    public string Address { get; set; }
    public int MemberCode { get; set; }
}
