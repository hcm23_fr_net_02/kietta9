﻿public class MobilePhone
{
    #region Properties
    public string Name { get; set; }
    public string Manufacturer { get; set; }
    public int StockQuantity { get; set; }
    public decimal Price { get; set; }
    public int ProductCode { get; set; }
    #endregion
}
